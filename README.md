# Parking

Parking est une une application Android (native) permettant de donner la disponibilité des parkings à Nantes. Elle permet plusieurs fonctionnalités :

* Donne la liste des parkings recensés par la ville de Nantes

* Donne des informations complémentaires sur les parkings + un visu avec une vue StreetView

* La possibilité de rechercher des parkings en particulier selon des critères prédéfinis dans l'application

* La gestion de favoris de parkings pour permettre d'afficher seulement les parkings préferés de l'utilisateurs

* Permet d'avoir la liste des activités (spectacles, musée, salle d'exposition, etc) à proximité

## Installation de l'application

### Comment transferer un fichier APK sur votre téléphone

Il s'agit ici de transférer un fichier .apk que vous aurez développé ou trouvé sur Internet depuis votre PC.

Tout d'abord, vous devez brancher votre smartphone ou tablette à votre ordinateur. Certains équipement Android demanderont pour cela d'être en mode "Débogage USB". Pour cela vous devez vous rendre dans vos paramètres. Et vous devriez trouverez le menu “Options pour les développeurs”. Cela permettra à votre smartphone d’être correctement reconnu.
Si les drivers de votre Android sont correctement installés sur votre ordinateur. Vous verrez votre périphérique de connecté.

Maintenant rendez-vous dans la mémoire interne de votre Android, puis dans le dossier "Download" (s'il n'existe pas, n'hésitez pas à le créer). Dans ce dossier, déposez le fichier .apk par un simple copier/coller ou glisser/déposer.

Maintenant que le fichier APK se trouve sur votre appareil, il ne reste plus qu'à l'installer. Pour cela, il ne sera pas nécessaire de laisser brancher votre Android sur votre ordinateur.

### Comment installer un fichier APK sur votre téléphone

La première chose à faire avant de pouvoir installer notre fichier APK est d’autoriser l’installation d’applications issues de sources inconnues c’est à dire qui ne proviennent pas du Google Play Store. Vous trouverez cette option dans la partie “Sécurité” des paramètres de votre appareil Android. Notez qu’après l’installation il est conseillé de désactiver cette option pour réduire les risques potentiels.
Une fois cette formalité effectuée. Il vous suffit ensuite d’utiliser un explorateur de fichiers pour retrouver le fichier en question. Cliquez alors sur le fichier APK et suivez les instructions qui s’affichent à l’écran.

## Environnement de développement

L'application est développée pour les téléphones Android. Le développement s'effectue avec les outils suivants :

* [Android Studio](https://developer.android.com/studio/index.html)

## Auteurs
* **Aurore Sancho** - *Développeuse* - [Linkedin](https://www.linkedin.com/in/aurore-sancho/)
* **Tanguy Helbert** - *Développeur*
* **Maxime Virey** - *Développeur*
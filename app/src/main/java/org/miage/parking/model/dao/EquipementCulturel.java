package org.miage.parking.model.dao;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Classe pour un EquipementCulturel
 * Cette classe implemente Parcelable pour pouvoir transformer une instance de cette classe
 * et la transferer via un intent
 */

@Table(name="EquipementCulturel")
public class EquipementCulturel extends Model implements Parcelable{

    public EquipementCulturel() {
        super();
    }

    @Expose
    @Column(name = "IdObj", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    @SerializedName("_IDOBJ")
    public int idObj;

    @Expose
    @Column(name = "Geo")
    @SerializedName("geo")
    public Geo geo;

    @Expose
    @SerializedName("_l")
    public List<Float> coordonnees;

    @Column(name = "Latitude")
    public Float latitude;

    @Column(name = "Longitude")
    public Float longitude;

    @Expose
    @Column(name = "LibTheme")
    @SerializedName("LIBTHEME")
    public String libTheme;

    @Expose
    @Column(name = "CodePostal")
    @SerializedName("CODE_POSTAL")
    public int codePostal;

    @Expose
    @Nullable
    @Column(name = "Web")
    @SerializedName("WEB")
    public String web;

    @Expose
    @Column(name = "Statut")
    @SerializedName("STATUT")
    public String statut;

    @Expose
    @Nullable
    @Column(name = "Telephone")
    @SerializedName("TELEPHONE")
    public String telephone;

    @Expose
    @Column(name = "Theme")
    @SerializedName("THEME")
    public int theme;


    @Expose
    @Column(name = "Commune")
    @SerializedName("COMMUNE")
    public String commune;

    @Expose
    @Column(name = "LibCategorie")
    @SerializedName("LIBCATEGORIE")
    public String libCategorie;

    @Expose
    @Column(name = "Adresse")
    @SerializedName("ADRESSE")
    public String adresse;

    @Expose
    @Nullable
    @Column(name = "LibType")
    @SerializedName("LIBTYPE")
    public String libType;

    @Expose
    @Nullable
    @Column(name = "Type")
    @SerializedName("TYPE")
    public int type;

    @Expose
    @Column(name = "Categorie")
    @SerializedName("CATEGORIE")
    public int categorie;

    @Column(name = "GeoName")
    public String geoName;

    public String getGeoName() {
        return geoName;
    }

    public void setGeoName(String geoName) {
        this.geoName = geoName;
    }

    public int getIdObj() {
        return idObj;
    }

    public void setIdObj(int idObj) {
        this.idObj = idObj;
    }

    public Geo getGeo() {
        return geo;
    }

    public void setGeo(Geo geo) {
        this.geo = geo;
    }

    public List<Float> getCoordonnees() {
        return coordonnees;
    }

    public void setCoordonnees(List<Float> coordonnees) {
        this.coordonnees = coordonnees;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public String getLibTheme() {
        return libTheme;
    }

    public void setLibTheme(String libTheme) {
        this.libTheme = libTheme;
    }

    public int getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(int codePostal) {
        this.codePostal = codePostal;
    }

    @Nullable
    public String getWeb() {
        return web;
    }

    public void setWeb(@Nullable String web) {
        this.web = web;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    @Nullable
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(@Nullable String telephone) {
        this.telephone = telephone;
    }

    public int getTheme() {
        return theme;
    }

    public void setTheme(int theme) {
        this.theme = theme;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getLibCategorie() {
        return libCategorie;
    }

    public void setLibCategorie(String libCategorie) {
        this.libCategorie = libCategorie;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    @Nullable
    public String getLibType() {
        return libType;
    }

    public void setLibType(@Nullable String libType) {
        this.libType = libType;
    }

    @Nullable
    public int getType() {
        return type;
    }

    public void setType(@Nullable int type) {
        this.type = type;
    }

    public int getCategorie() {
        return categorie;
    }

    public void setCategorie(int categorie) {
        this.categorie = categorie;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idObj);
        dest.writeString(geoName);
        dest.writeString(adresse);
        dest.writeFloat(latitude);
        dest.writeFloat(longitude);
        dest.writeString(libCategorie);
        dest.writeString(libTheme);
        dest.writeString(statut);
        dest.writeString(telephone);
        dest.writeString(commune);
        dest.writeInt(codePostal);
        dest.writeString(web);
        dest.writeString(libType);

    }

    public EquipementCulturel(Parcel in) {
        this.idObj = in.readInt();
        this.geoName = in.readString();
        this.adresse = in.readString();
        this.latitude = in.readFloat();
        this.longitude = in.readFloat();
        this.libCategorie = in.readString();
        this.libTheme = in.readString();
        this.statut = in.readString();
        this.telephone = in.readString();
        this.commune = in.readString();
        this.codePostal = in.readInt();
        this.web = in.readString();
        this.libType = in.readString();
    }

    public EquipementCulturel(int idObj,String geoName, String adresse, float latitude, float longitude,String libCategorie, String libTheme, String statut, String telephone, String commune, int codePostal, String web, String libType) {
        this.idObj = idObj;
        this.geoName = geoName;
        this.adresse = adresse;
        this.latitude = latitude;
        this.longitude = longitude;
        this.libCategorie = libCategorie;
        this.libTheme = libTheme;
        this.statut = statut;
        this.telephone = telephone;
        this.commune = commune;
        this.codePostal = codePostal;
        this.web = web;
        this.libType = libType;
    }

    public static final Parcelable.Creator<EquipementCulturel> CREATOR = new Parcelable.Creator<EquipementCulturel>()
    {
        @Override
        public EquipementCulturel createFromParcel(Parcel source)
        {
            return new EquipementCulturel(source);
        }

        @Override
        public EquipementCulturel[] newArray(int size)
        {
            return new EquipementCulturel[size];
        }
    };
}
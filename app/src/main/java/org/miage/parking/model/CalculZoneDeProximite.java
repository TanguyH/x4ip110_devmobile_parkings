package org.miage.parking.model;

/**
 * Classe permettant de definir les limites d'une zone autour d'un point
 */

public class CalculZoneDeProximite {
    private double latitude;
    private double longitude;
    private double rayon;
    private double maxLatitude;
    private double minLatitude;
    private double maxLongitude;
    private double minLongitude;

    public CalculZoneDeProximite(double latitude, double longitude, double rayon) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.rayon = rayon;
        calculerZone();
    }

    /**
     * Permet de calculer une zone en fonction d'un rayon, d'une latitude et d'une longitude
     */
    private void calculerZone() {
        // d'après mes sources :
        // 1° de latitude = 111,11 Km
        // Produit en croix pour trouver le nombre de degrés de latitude auquel correspond la longueur de notre rayon
        double offSetLat = rayon / 111110d;

        // on calcul pour la longitude (en passant en radian)
        double oneLongitudeDegree = 111110 * Math.cos(latitude * Math.PI / 180);

        // Produit en croix pour trouver le nombre de degrés de longitude auquel correspond la longueur de notre rayon
        double offSetLong = rayon / oneLongitudeDegree;
        maxLatitude = latitude + offSetLat;
        minLatitude = latitude - offSetLat;
        maxLongitude = longitude + offSetLong;
        minLongitude = longitude - offSetLong;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getRayon() {
        return rayon;
    }

    public void setRayon(double rayon) {
        this.rayon = rayon;
    }

    public double getMaxLatitude() {
        return maxLatitude;
    }

    public void setMaxLatitude(double maxLatitude) {
        this.maxLatitude = maxLatitude;
    }

    public double getMinLatitude() {
        return minLatitude;
    }

    public void setMinLatitude(double minLatitude) {
        this.minLatitude = minLatitude;
    }

    public double getMaxLongitude() {
        return maxLongitude;
    }

    public void setMaxLongitude(double maxLongitude) {
        this.maxLongitude = maxLongitude;
    }

    public double getMinLongitude() {
        return minLongitude;
    }

    public void setMinLongitude(double minLongitude) {
        this.minLongitude = minLongitude;
    }
}

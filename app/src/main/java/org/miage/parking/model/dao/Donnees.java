package org.miage.parking.model.dao;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * La classe representant un element du niveau 3 dans le parsing de l'appel REST aux Disponibilités
 * qui contient les informations de ces dernières
 */
public class Donnees {

    @Expose
    @SerializedName("Groupes_Parking")
    public GroupesParking groupesParking;
}

package org.miage.parking.model.dao;

import android.support.annotation.Nullable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Classe pour un Parking
 */

@Table(name="Parking")
public class Parking extends Model{

    public Parking() {
        super();
    }

    @Expose
    @Column(name = "IdObj", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    @SerializedName("_IDOBJ")
    public int idObj;

    @Expose
    @Column(name = "ServiceVelo")
    @SerializedName("SERVICE_VELO")
    public String serviceVelo;

    @Expose
    @Column(name = "CapaciteVoiture")
    @SerializedName("CAPACITE_VOITURE")
    public int capaciteVoiture;

    @Expose
    @Column(name = "StationnementVelo")
    @SerializedName("STATIONNEMENT_VELO")
    public boolean stationnementVelo;

    @Expose
    @Column(name = "StationnementVeloSecurise")
    @SerializedName("STATIONNEMENT_VELO_SECURISE")
    public boolean stationnementVeloSecurise;

    @Expose
    @Column(name = "Geo")
    @SerializedName("geo")
    public Geo geo;

    @Expose
    @SerializedName("_l")
    public List<Float> coordonnees;

    @Column(name = "Latitude")
    public Float latitude;

    @Column(name = "Longitude")
    public Float longitude;

    @Expose
    @Column(name = "CapaciteVehiculeElectrique")
    @SerializedName("CAPACITE_VEHICULE_ELECTRIQUE")
    public Integer capaciteVehiculeElectrique;

    @Expose
    @Column(name = "CodePostal")
    @SerializedName("CODE_POSTAL")
    public int codePostal;

    @Expose
    @Column(name = "AccesPMR")
    @SerializedName("ACCES_PMR")
    public boolean accesPMR;

    @Expose
    @Column(name = "Telephone")
    @SerializedName("TELEPHONE")
    public String telephone;

    @Expose
    @SerializedName("MOYEN_PAIEMENT")
    public String moyenPaiement;

    @Expose
    @Column(name = "AccesTransportsCommuns")
    @SerializedName("ACCES_TRANSPORTS_COMMUNS")
    public String accesTransportsCommuns;

    @Expose
    @Column(name = "CapaciteMoto")
    @SerializedName("CAPACITE_MOTO")
    public Integer capaciteMoto;

    @Expose
    @Column(name = "ConditionsAcces")
    @SerializedName("CONDITIONS_D_ACCES")
    public String conditionsAcces;

    @Expose
    @Column(name = "Commune")
    @SerializedName("COMMUNE")
    public String commune;

    @Expose
    @Column(name = "CapaciteVelo")
    @SerializedName("CAPACITE_VELO")
    public int capaciteVelo;

    @Expose
    @Column(name = "Presentation")
    @SerializedName("PRESENTATION")
    public String presentation;

    @Expose
    @Column(name = "CapacitePMR")
    @SerializedName("CAPACITE_PMR")
    public int capacitePMR;

    @Expose
    @Column(name = "LibCategorie")
    @SerializedName("LIBCATEGORIE")
    public String libCategorie;

    @Expose
    @Column(name = "Exploitant")
    @SerializedName("EXPLOITANT")
    public String exploitant;

    @Expose
    @Column(name = "SiteWeb")
    @SerializedName("SITE_WEB")
    public String siteWeb;

    @Expose
    @Column(name = "Adresse")
    @SerializedName("ADRESSE")
    public String adresse;

    @Expose
    @Column(name = "LibType")
    @SerializedName("LIBTYPE")
    public String libType;

    @Expose
    @Column(name = "Twitter")
    @SerializedName("TWITTER")
    @Nullable
    public String twitter;

    @Expose
    @Column(name = "Services")
    @SerializedName("SERVICES")
    @Nullable
    public String services;

    @Expose
    @Column(name = "AutresServicesMobilite")
    @SerializedName("AUTRES_SERVICE_MOB_PROX")
    @Nullable
    public String autresServicesMobi;

    @Column(name = "GeoName")
    public String geoName;

    @Column(name= "MoyenDePaiement")
    public MoyenDePaiement moyenDePaiement;

    @Expose
    @Column(name = "Disponibilite")
    public Disponibilite disponibilite;

    @Column(name = "IsParkingRelais")
    public boolean isParkingRelais;

    public Disponibilite getDisponibilite() {
        return disponibilite;
    }

    public void setDisponibilite(Disponibilite disponibilite) {
        this.disponibilite = disponibilite;
    }

    public String getServiceVelo() {
        return serviceVelo;
    }

    public int getCapaciteVoiture() {
        return capaciteVoiture;
    }

    public boolean isStationnementVelo() {
        return stationnementVelo;
    }

    public boolean isStationnementVeloSecurise() {return stationnementVeloSecurise;}

    public void setGeoName(String geoName) {
        this.geoName = geoName;
    }

    public String getGeoName() {
        return geoName;
    }

    public void setGeo(Geo geo) {
        this.geo = geo;
    }

    public Geo getGeo() {
        return geo;
    }

    public List<Float> getCoordonnees() {
        return coordonnees;
    }

    public void setCoordonnees(List<Float> coordonnees) {
        this.coordonnees = coordonnees;
    }

    public Float getLatitude() {
        return latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Integer getCapaciteVehiculeElectrique() {
        return capaciteVehiculeElectrique;
    }

    public int getCodePostal() {return codePostal;}

    public boolean isAccesPMR() {return accesPMR;}

    public String getTelephone() {
        return telephone;
    }

    public String getMoyenPaiement() {
        return moyenPaiement;
    }

    public String getAccesTransportsCommuns() {
        return accesTransportsCommuns;
    }

    public Integer getCapaciteMoto() {return capaciteMoto;}

    public String getConditionsAcces() {
        return conditionsAcces;
    }

    public String getCommune() {
        return commune;
    }

    public int getCapaciteVelo() {return capaciteVelo;}

    public String getPresentation() {
        return presentation;
    }

    public int getCapacitePMR() {
        return capacitePMR;
    }

    public String getLibCategorie() {
        return libCategorie;
    }

    public String getExploitant() {
        return exploitant;
    }

    public String getSiteWeb() {
        return siteWeb;
    }

    public String getAdresse() {
        return adresse;
    }

    public String getLibType() {
        return libType;
    }

    public int getIdObj() {return idObj;}

    public void setServiceVelo(String serviceVelo) {
        this.serviceVelo = serviceVelo;
    }

    public void setCapaciteVoiture(int capaciteVoiture) {this.capaciteVoiture = capaciteVoiture;}

    public void setStationnementVelo(boolean stationnementVelo) {this.stationnementVelo = stationnementVelo;}

    public void setStationnementVeloSecurise(boolean stationnementVeloSecurise) {this.stationnementVeloSecurise = stationnementVeloSecurise;}

    public void setCapaciteVehiculeElectrique(Integer capaciteVehiculeElectrique) {this.capaciteVehiculeElectrique = capaciteVehiculeElectrique;}

    public void setCodePostal(int codePostal) {
        this.codePostal = codePostal;
    }

    public void setAccesPMR(boolean accesPMR) {
        this.accesPMR = accesPMR;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setMoyenPaiement(String moyenPaiement) {this.moyenPaiement = moyenPaiement;}

    public void setAccesTransportsCommuns(String accesTransportsCommuns) {this.accesTransportsCommuns = accesTransportsCommuns;}

    public void setCapaciteMoto(Integer capaciteMoto) {
        this.capaciteMoto = capaciteMoto;
    }

    public void setConditionsAcces(String conditionsAcces) {this.conditionsAcces = conditionsAcces;}

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public void setCapaciteVelo(int capaciteVelo) {this.capaciteVelo = capaciteVelo;}

    public void setPresentation(String presentation) {this.presentation = presentation;}

    public void setCapacitePMR(int capacitePMR) {
        this.capacitePMR = capacitePMR;
    }

    public void setLibCategorie(String libCategorie) {
        this.libCategorie = libCategorie;
    }

    public void setExploitant(String exploitant) {
        this.exploitant = exploitant;
    }

    public void setSiteWeb(String siteWeb) {this.siteWeb = siteWeb;}

    public void setAdresse(String adresse) {this.adresse = adresse;}

    public void setLibType(String libType) {this.libType = libType;}

    public void setIdObj(int idObj) {this.idObj = idObj;}

    @Nullable
    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(@Nullable String twitter) {
        this.twitter = twitter;
    }

    @Nullable
    public String getServices() {
        return services;
    }

    public void setServices(@Nullable String services) {
        this.services = services;
    }

    public boolean isParkingRelais() {
        return isParkingRelais;
    }

    public void setIsParkingRelais(boolean isParkingRelais) { this.isParkingRelais = isParkingRelais;}

    @Nullable
    public String getAutresServicesMobi() {
        return autresServicesMobi;
    }

    public void setAutresServicesMobi(@Nullable String autresServicesMobi) {
        this.autresServicesMobi = autresServicesMobi;
    }

    public int getCapaciteTotale(){
        return ((capaciteMoto != null) ? capaciteMoto : 0)
                + capacitePMR
                + ((capaciteVehiculeElectrique != null) ? capaciteVehiculeElectrique : 0)
                + capaciteVelo
                + capaciteVoiture;
    }

    public MoyenDePaiement getMoyenDePaiement() {
        return moyenDePaiement;
    }

    public void setMoyenDePaiement(MoyenDePaiement moyenDePaiement) {
        this.moyenDePaiement = moyenDePaiement;
    }

    public String getAdresseTotal(){
        return this.getAdresse() + "\n" + this.getCodePostal() + " " + this.getCommune();
    }

    public String getAdresseTotalOneLine(){
        return this.getAdresse() + ", " + this.getCodePostal() + " " + this.getCommune();
    }
}
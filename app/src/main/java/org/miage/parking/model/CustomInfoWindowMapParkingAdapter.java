package org.miage.parking.model;

import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import org.miage.parking.R;

/**
 * Classe permettant de créer des infoWindows (marker) personnalisés
 */

public class CustomInfoWindowMapParkingAdapter implements GoogleMap.InfoWindowAdapter{

    private final View mWindow;
    private Context mContext;
    private static final String TYPE_PARKING = "PARKING";
    private static final String TYPE_EQUIPEMENT = "EQUIPEMENT";

    public CustomInfoWindowMapParkingAdapter(Context context) {
        mContext = context;
        mWindow = LayoutInflater.from(mContext).inflate(R.layout.map_parking_infowindow, null);
    }

    /**
     * Permet de mettre a jour les informations de la vue a partir du snippet du marker
     * Le snippet du marker contient toutes les informations du parking
     * @param marker le marker dont on veut modifier l'infoView
     * @param view la view qui sera mise à jour
     */
    private void miseAJourDesInfos(Marker marker, View view) {
        TextView parkingTitleTextView = view.findViewById(R.id.map_parking_nom);
        View parkingBorder = view.findViewById(R.id.map_parking_border);
        TextView parkingInfoAdresse = view.findViewById(R.id.map_parking_adresse);
        TextView parkingInfoDispo = view.findViewById(R.id.map_parking_dispo);
        CheckBox parkingInfoFavoris = view.findViewById(R.id.map_parking_isFavorite);
        TextView parkingInfoMoyenDePaiement = view.findViewById(R.id.map_parking_moyens_paiment);
        TextView equipementTitleTextView = view.findViewById(R.id.map_equipement_nom);
        View equipementBorder = view.findViewById(R.id.map_equipement_border);
        TextView equipementInfoAdresse = view.findViewById(R.id.map_equipement_adresse);
        TextView equipementInfoSite = view.findViewById(R.id.map_equipement_site);
        TextView equipementInfoTelephone = view.findViewById(R.id.map_equipement_telephone);

        // on split le snippet pour recuperer toutes les informations
        String[] infos = marker.getSnippet().split(getContext().getString(R.string.SEPARATEUR_SNIPPET));

        // on recupere les differentes informations
        switch(infos[0]){
            case TYPE_PARKING :
                parkingTitleTextView.setText(marker.getTitle());
                // en fonction de la longueur du nom, on adapte la taille de la police pour que ca tienne sur une ligne
                if(parkingTitleTextView.getText().length() > 34) {
                    parkingTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, (680 / parkingTitleTextView.getText().length()));
                } else {
                    parkingTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                }

                parkingInfoAdresse.setText(infos[1]);
                parkingInfoDispo.setText(infos[2]);
                parkingInfoFavoris.setChecked(Boolean.valueOf(infos[3]));

                StringBuilder builderMp = new StringBuilder();
                if(!infos[4].equals(getContext().getString(R.string.moyen_de_paiement_inconnu))) {
                    builderMp.append(getContext().getString(R.string.details_moyen_de_paiement));
                    builderMp.append(" : ");
                    builderMp.append(infos[4]);
                    parkingInfoMoyenDePaiement.setText(builderMp.toString());
                } else {
                    parkingInfoMoyenDePaiement.setText(infos[4]);
                }

                parkingTitleTextView.setVisibility(View.VISIBLE);
                parkingInfoAdresse.setVisibility(View.VISIBLE);
                parkingInfoDispo.setVisibility(View.VISIBLE);
                parkingInfoFavoris.setVisibility(View.VISIBLE);
                parkingInfoMoyenDePaiement.setVisibility(View.VISIBLE);
                parkingBorder.setVisibility(View.VISIBLE);

                equipementTitleTextView.setVisibility(View.GONE);
                equipementInfoAdresse.setVisibility(View.GONE);
                equipementInfoSite.setVisibility(View.GONE);
                equipementInfoTelephone.setVisibility(View.GONE);
                equipementBorder.setVisibility(View.GONE);
                break;
            case TYPE_EQUIPEMENT :
                // on recupere le nom de l'equipement
                equipementTitleTextView.setText(marker.getTitle());
                // en fonction de la longueur du nom, on adapte la taille de la police pour que ca tienne sur une ligne
                if(equipementTitleTextView.getText().length() > 34) {
                    equipementTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, (680 / equipementTitleTextView.getText().length()));
                } else {
                    equipementTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                }

                // on recupere l'adresse
                StringBuilder builder = new StringBuilder();
                builder.append(infos[1]);
                builder.append(", ");
                builder.append(infos[2]);
                builder.append(" ");
                builder.append(infos[3]);
                equipementInfoAdresse.setText(builder.toString());

                // on recupere le site web s'il existe
                if (infos[4].trim().isEmpty()) {
                    equipementInfoSite.setText(getContext().getString(R.string.site_inconnu));
                    equipementInfoSite.setTypeface(equipementInfoSite.getTypeface(), Typeface.ITALIC);
                } else {
                    equipementInfoSite.setText(infos[4]);
                }

                // on recupere le numéro de telephone s'il existe
                if (infos[5].trim().isEmpty()) {
                    equipementInfoTelephone.setText(getContext().getString(R.string.telephone_inconnu));
                    equipementInfoTelephone.setTypeface(equipementInfoTelephone.getTypeface(), Typeface.ITALIC);
                } else {
                    equipementInfoTelephone.setText(infos[5]);
                }


                parkingTitleTextView.setVisibility(View.GONE);
                parkingInfoAdresse.setVisibility(View.GONE);
                parkingInfoDispo.setVisibility(View.GONE);
                parkingInfoFavoris.setVisibility(View.GONE);
                parkingInfoMoyenDePaiement.setVisibility(View.GONE);
                parkingBorder.setVisibility(View.GONE);

                equipementTitleTextView.setVisibility(View.VISIBLE);
                equipementInfoAdresse.setVisibility(View.VISIBLE);
                equipementInfoSite.setVisibility(View.VISIBLE);
                equipementInfoTelephone.setVisibility(View.VISIBLE);
                equipementBorder.setVisibility(View.VISIBLE);
                break;
             default:
                 break;
        }
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        miseAJourDesInfos(marker, mWindow);
        return mWindow;
    }

    public View getWindow() {
        return mWindow;
    }

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context mContext) {
        this.mContext = mContext;
    }
}

package org.miage.parking.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import org.miage.parking.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Classe permettant d'afficher les cases dans le menu principal
 */

public class MainActivityAdapter extends BaseAdapter {
    private Context mContext;
    private CaseMenu[] cases;

    @BindView(R.id.caseMenu_image)
    ImageView imageView;
    @BindView(R.id.caseMenu_name)
    TextView nameTextView;

    public MainActivityAdapter(Context context, CaseMenu[] cases) {
        this.mContext = context;
        this.cases = cases;
    }

    @Override
    public int getCount() {
        return cases.length;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final CaseMenu caseMenu = cases[position];

        // view holder pattern
        if (convertView == null) {
            final LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.linearlayout_case, null);
        }
        ButterKnife.bind(this, convertView);

        imageView.setImageResource(caseMenu.getImageResource());
        nameTextView.setText(mContext.getString(caseMenu.getName()));

        return convertView;
    }
}

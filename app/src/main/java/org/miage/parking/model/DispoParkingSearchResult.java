package org.miage.parking.model;

import com.google.gson.annotations.Expose;
import org.miage.parking.model.dao.Opendata;

/**
 * Classe pour le parsing de l'appel REST pour les disponibilites des parkings
 */
public class DispoParkingSearchResult {
    @Expose
    public Opendata opendata;
}

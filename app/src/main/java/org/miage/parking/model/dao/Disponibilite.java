package org.miage.parking.model.dao;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Classe de Disponibilite
 */

@Table(name="Disponibilite")
public class Disponibilite extends Model {

    public Disponibilite() {
        super();
    }

    @Expose
    @SerializedName("Grp_identifiant")
    @Column(name = "GrpIdentifiant", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public int grpIdentifiant;

    @Expose
    @SerializedName("Grp_nom")
    @Column(name = "GrpNom")
    public String grpNom;

    @Expose
    @SerializedName("Grp_statut")
    @Column(name = "GrpStatut")
    public int grpStatut;

    @Expose
    @SerializedName("Grp_pri_aut")
    @Column(name = "GrpPriAut")
    public int grpPriAut;

    @Expose
    @SerializedName("Grp_disponible")
    @Column(name = "GrpDisponible")
    public int grpDisponible;

    @Expose
    @SerializedName("Grp_complet")
    @Column(name = "GrpComplet")
    public int grpComplet;

    @Expose
    @SerializedName("Grp_exploitation")
    @Column(name = "GrpExploitation")
    public int grpExploitation;

    @Expose
    @SerializedName("Grp_horodatage")
    @Column(name = "GrpHorodatage")
    public String grpHorodatage;

    @Column(name = "Favoris")
    public Boolean isFavoris;

    @Expose
    @Column(name = "IdObj")
    @SerializedName("IdObj")
    public int idObj;

    @Expose
    @Column(name = "Parking")
    public Parking parking;

    public Parking getParking() {
        return parking;
    }

    public void setParking(Parking parking) {
        this.parking = parking;
    }

    public int getGrpIdentifiant() {
        return grpIdentifiant;
    }

    public void setGrpIdentifiant(int grpIdentifiant) {
        this.grpIdentifiant = grpIdentifiant;
    }

    public String getGrpNom() {
        return grpNom;
    }

    public void setGrpNom(String grpNom) {
        this.grpNom = grpNom;
    }

    public int getGrpStatut() {
        return grpStatut;
    }

    public void setGrpStatut(int grpStatut) {
        this.grpStatut = grpStatut;
    }

    public int getGrpPriAut() {
        return grpPriAut;
    }

    public void setGrpPriAut(int grpPriAut) {
        this.grpPriAut = grpPriAut;
    }

    public int getGrpDisponible() {
        return grpDisponible;
    }

    public void setGrpDisponible(int grpDisponible) {
        this.grpDisponible = grpDisponible;
    }

    public int getGrpComplet() {
        return grpComplet;
    }

    public void setGrpComplet(int grpComplet) {
        this.grpComplet = grpComplet;
    }

    public int getGrpExploitation() {
        return grpExploitation;
    }

    public void setGrpExploitation(int grpExploitation) {
        this.grpExploitation = grpExploitation;
    }

    public String getGrpHorodatage() {
        return grpHorodatage;
    }

    public void setGrpHorodatage(String grpHorodatage) {
        this.grpHorodatage = grpHorodatage;
    }

    public Boolean getIsFavoris() {
        return isFavoris;
    }

    public void setIsFavoris(Boolean favoris) {
        isFavoris = favoris;
    }

    public int getIdObj() {
        return this.idObj;
    }

    public void setIdObj(int idObj) {
        this.idObj = idObj;
    }
}

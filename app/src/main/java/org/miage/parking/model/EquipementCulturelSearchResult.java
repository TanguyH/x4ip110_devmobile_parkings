package org.miage.parking.model;

import com.google.gson.annotations.Expose;
import org.miage.parking.model.dao.EquipementCulturel;
import java.util.List;

/**
 * Classe pour le parsing de l'appel REST pour les equipements culturels
 */
public class EquipementCulturelSearchResult {
    @Expose
    public List<EquipementCulturel> data;
}


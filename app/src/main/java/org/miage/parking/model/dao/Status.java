package org.miage.parking.model.dao;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * La classe representant un element du niveau 3 dans le parsing de l'appel REST aux Disponibilités
 */
public class Status {

    @Expose
    @SerializedName("code")
    public String code;

    @Expose
    @SerializedName("message")
    public String message;
}

package org.miage.parking.model;

import com.google.gson.annotations.Expose;
import org.miage.parking.model.dao.Parking;
import java.util.List;

/**
 * Classe pour le parsing de l'appel REST pour les parkings
 */
public class ParkingSearchResult {
    @Expose
    public List<Parking> data;
}

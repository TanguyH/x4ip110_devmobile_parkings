package org.miage.parking.model;

/**
 * Classe permettant de créer les cases qui seront visible dans le gridView du Menu
 */

public class CaseMenu {
    private int name;
    private int imageResource;

    public CaseMenu(int name, int imageResource) {
        this.name = name;
        this.imageResource = imageResource;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public int getImageResource() {
        return imageResource;
    }

    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
    }

}

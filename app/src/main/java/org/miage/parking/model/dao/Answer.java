package org.miage.parking.model.dao;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * La classe representant le deuxieme niveau dans le parsing de l'appel REST aux Disponibilités
 */
public class Answer {

    @Expose
    @SerializedName("status")
    public Status status;

    @Expose
    @SerializedName("data")
    public Donnees data;
}

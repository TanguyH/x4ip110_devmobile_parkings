package org.miage.parking.model;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;

import org.miage.parking.R;
import org.miage.parking.model.dao.Disponibilite;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Classe servant a mapper les informations des Disponibilites vers la View
 */
public class DispoParkingRelaisAdapter extends ArrayAdapter<Disponibilite> {
    @BindView(R.id.details_parking_adapter_nom)
    TextView dispoParkingNomTextView;

    @BindView(R.id.details_parking_adapter_presentationText)
    TextView dispoParkingPresentationTextView;

    @BindView(R.id.details_parking_adapter_adresse)
    TextView dispoParkingAdresseTextView;

    @BindView(R.id.details_parking_adapter_twitter)
    TextView dispoParkingTwitterTextView;

    @BindView(R.id.details_parking_adapter_web)
    TextView dispoParkingWebTextView;

    @BindView(R.id.details_parking_adapter_telephone)
    TextView dispoParkingTelephoneTextView;

    @BindView(R.id.details_parking_adapter_switchText_text)
    TextView dispoParkingSwitchTextViewText;

    @BindView(R.id.details_parking_adapter_mp)
    TextView dispoParkingMoyenDePaiementTextView;

    @BindView(R.id.details_parking_adapter_service_velo)
    TextView dispoParkingServiceVeloTextView;

    @BindView(R.id.details_parking_adapter_services)
    TextView dispoParkingServicesTextView;

    @BindView(R.id.details_parking_adapter_condition_acces)
    TextView dispoParkingConditionAccesTextView;

    @BindView(R.id.details_parking_adapter_autres_services_mobi)
    TextView dispoParkingAutresServicesMobiTextView;

    @BindView(R.id.details_parking_adapter_isFavorite)
    CheckBox mIsFavorite;

    @BindView(R.id.details_parking_adapter_bike)
    CheckBox mIsBike;

    @BindView(R.id.details_parking_adapter_moto)
    CheckBox mIsMoto;

    @BindView(R.id.details_parking_adapter_pmr)
    CheckBox mIsPMR;

    @BindView(R.id.details_parking_adapter_electricCar)
    CheckBox mIsElectricCar;

    Disponibilite dispo;

    public DispoParkingRelaisAdapter(Context context, List<Disponibilite> disponibilites){
        super(context, -1, disponibilites);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View actualView = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            actualView = inflater.inflate(R.layout.details_parking_relais_adapter, parent, false);
        }
        ButterKnife.bind(this, actualView);
        dispo = getItem(position);

        if(dispo.getParking() != null) {
            // on recupere le nom
            dispoParkingNomTextView.setText(dispo.getParking().getGeoName());
            // si le nom depasse les 20 caracteres (qui est la limite pour l'affichage avec un textsize de 32sp
            if (dispoParkingNomTextView.getText().length() > 20) {
                // on reduit le moins possible la textSize pour permettre le bon affichage
                // 640 etant la valeur max estimee en prenant en compte un nom de 20 caracteres en taille 32sp
                dispoParkingNomTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, (640 / dispoParkingNomTextView.getText().length()));
            }

            dispoParkingPresentationTextView.setText(dispo.getParking().getPresentation());

            // on recupere les differentes informations
            dispoParkingSwitchTextViewText.setText(getContext().getString(R.string.details_favoris));

            //pas possible de cliquer sur les checkbox
            mIsBike.setEnabled(false);
            mIsMoto.setEnabled(false);
            mIsPMR.setEnabled(false);
            mIsElectricCar.setEnabled(false);

            //maj de la valeur des checkbox
            mIsBike.setChecked(dispo.getParking().isStationnementVelo());
            mIsMoto.setChecked(dispo.getParking().getCapaciteMoto() != null);
            mIsPMR.setChecked(dispo.getParking().isAccesPMR() || dispo.getParking().getCapacitePMR() > 0);
            mIsElectricCar.setChecked(dispo.getParking().getCapaciteVehiculeElectrique() != null);

            // adresse
            dispoParkingAdresseTextView.setText(dispo.getParking().getAdresseTotalOneLine());

            // twitter
            if(dispo.getParking().getTwitter() == null || dispo.getParking().getTwitter().isEmpty()) {
                dispoParkingTwitterTextView.setText(getContext().getString(R.string.twitter_inconnu));
                dispoParkingTwitterTextView.setTypeface(dispoParkingTwitterTextView.getTypeface(), Typeface.ITALIC);
            } else {
                dispoParkingTwitterTextView.setText(dispo.getParking().getTwitter());
            }

            // web
            if(dispo.getParking().getSiteWeb() == null || dispo.getParking().getSiteWeb().isEmpty()) {
                dispoParkingWebTextView.setText(getContext().getString(R.string.site_inconnu));
                dispoParkingWebTextView.setTypeface(dispoParkingWebTextView.getTypeface(), Typeface.ITALIC);
            } else {
                dispoParkingWebTextView.setText(dispo.getParking().getSiteWeb());
            }

            // telephone
            if(dispo.getParking().getTelephone() == null || dispo.getParking().getTelephone().isEmpty()) {
                dispoParkingTelephoneTextView.setText(getContext().getString(R.string.telephone_inconnu));
                dispoParkingTelephoneTextView.setTypeface(dispoParkingTelephoneTextView.getTypeface(), Typeface.ITALIC);
            } else {
                dispoParkingTelephoneTextView.setText(dispo.getParking().getTelephone());
            }

            // moyens de paiement
            if(dispo.getParking().getMoyenDePaiement() != null && !dispo.getParking().getMoyenDePaiement().toString().equals(getContext().getString(R.string.moyen_de_paiement_inconnu))){
                SpannableString moyen = new SpannableString(getContext().getString(R.string.details_moyen_de_paiement));
                moyen.setSpan(new StyleSpan(Typeface.BOLD),0,moyen.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                SpannableStringBuilder builderMoyen = new SpannableStringBuilder();
                builderMoyen.append(moyen);
                builderMoyen.append(" : ");
                builderMoyen.append(dispo.getParking().getMoyenDePaiement().toString());
                dispoParkingMoyenDePaiementTextView.setText(builderMoyen);
            } else {
                dispoParkingMoyenDePaiementTextView.setText(getContext().getString(R.string.moyen_de_paiement_inconnu));
                dispoParkingMoyenDePaiementTextView.setTypeface(dispoParkingMoyenDePaiementTextView.getTypeface(), Typeface.ITALIC);
            }

            // service velo
            if(dispo.getParking().getServiceVelo() == null || dispo.getParking().getServiceVelo().isEmpty()){
                dispoParkingServiceVeloTextView.setText(getContext().getString(R.string.service_velo_inconnu));
                dispoParkingServiceVeloTextView.setTypeface(dispoParkingServiceVeloTextView.getTypeface(), Typeface.ITALIC);
            } else {
                SpannableString serviceVelo = new SpannableString(getContext().getString(R.string.details_service_velo));
                serviceVelo.setSpan(new StyleSpan(Typeface.BOLD),0,serviceVelo.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                SpannableStringBuilder builderServiceVelo = new SpannableStringBuilder();
                builderServiceVelo.append(serviceVelo);
                builderServiceVelo.append(" : ");
                builderServiceVelo.append(dispo.getParking().getServiceVelo());
                dispoParkingServiceVeloTextView.setText(builderServiceVelo);
            }

            // service
            if(dispo.getParking().getServices() == null || dispo.getParking().getServices().isEmpty()){
                dispoParkingServicesTextView.setText(getContext().getString(R.string.services_inconnu));
                dispoParkingServicesTextView.setTypeface(dispoParkingServicesTextView.getTypeface(), Typeface.ITALIC);
            } else {
                SpannableString services = new SpannableString(getContext().getString(R.string.details_services));
                services.setSpan(new StyleSpan(Typeface.BOLD),0,services.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                SpannableStringBuilder builderServices = new SpannableStringBuilder();
                builderServices.append(services);
                builderServices.append(" : ");
                builderServices.append(dispo.getParking().getServices());
                dispoParkingServicesTextView.setText(builderServices);
            }

            // condition d'acces
            if(dispo.getParking().getConditionsAcces() == null || dispo.getParking().getConditionsAcces().isEmpty()){
                dispoParkingConditionAccesTextView.setText(getContext().getString(R.string.condition_d_acces_inconnu));
                dispoParkingConditionAccesTextView.setTypeface(dispoParkingConditionAccesTextView.getTypeface(), Typeface.ITALIC);
                dispoParkingConditionAccesTextView.setVisibility(View.GONE);
            } else {
                SpannableString condition = new SpannableString(getContext().getString(R.string.details_condition_d_acces));
                condition.setSpan(new StyleSpan(Typeface.BOLD),0,condition.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                SpannableStringBuilder builderCondition = new SpannableStringBuilder();
                builderCondition.append(condition);
                builderCondition.append(" : ");
                builderCondition.append(dispo.getParking().getConditionsAcces());
                dispoParkingConditionAccesTextView.setText(builderCondition);
            }

            // autres services mobilite
            if(dispo.getParking().getAutresServicesMobi() == null || dispo.getParking().getAutresServicesMobi().isEmpty()){
                dispoParkingAutresServicesMobiTextView.setText(getContext().getString(R.string.autres_services_mobi_inconnu));
                dispoParkingAutresServicesMobiTextView.setTypeface(dispoParkingAutresServicesMobiTextView.getTypeface(), Typeface.ITALIC);
            } else {
                SpannableString autreServiceMobi = new SpannableString(getContext().getString(R.string.details_autre_service_mobi));
                autreServiceMobi.setSpan(new StyleSpan(Typeface.BOLD),0,autreServiceMobi.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                SpannableStringBuilder builderAutreServiceMobi = new SpannableStringBuilder();
                builderAutreServiceMobi.append(autreServiceMobi);
                builderAutreServiceMobi.append(" : ");
                builderAutreServiceMobi.append(dispo.getParking().getAutresServicesMobi());
                dispoParkingAutresServicesMobiTextView.setText(builderAutreServiceMobi);
            }

            // partie favoris
            mIsFavorite.setChecked(dispo.getIsFavoris());

            /* Regarder pour save l'update du Favoris*/
            mIsFavorite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    ActiveAndroid.beginTransaction();
                    if (isChecked) {
                        dispo.setIsFavoris(true);
                    } else {
                        dispo.setIsFavoris(false);
                    }

                    dispo.save();

                    ActiveAndroid.setTransactionSuccessful();
                    ActiveAndroid.endTransaction();
                }
            });
        }

        return actualView;
    }
}
package org.miage.parking.model;

import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import org.miage.parking.R;

/**
 * Classe permettant de créer des infoWindows (marker) personnalisés pour les Equipements
 * Cette classe est utilisée sur la MapEquipementCulturelActivity pour les infoView des equipements,
 * cependant sur cet ecran on affiche egalement un parking qui est gerer dans la méthode de mise à jour.
 */

public class CustomInfoWindowMapEquipementAdapter implements GoogleMap.InfoWindowAdapter{

    private final View mWindow;
    private Context mContext;

    private static final String TYPE_PARKING = "PARKING";
    private static final String TYPE_EQUIPEMENT = "EQUIPEMENT";

    public CustomInfoWindowMapEquipementAdapter(Context context) {
        mContext = context;
        mWindow = LayoutInflater.from(mContext).inflate(R.layout.map_equipement_infowindow, null);
    }

    /**
     * Permet de mettre a jour les informations de la vue a partir du snippet du marker
     * Le snippet du marker contient toutes les informations de l'equipement culturel
     * @param marker le marker dont on veut modifier l'infoView
     * @param view la view qui sera mise à jour
     */
    private void miseAJourDesInfos(Marker marker, View view) {
        TextView titleTextView = view.findViewById(R.id.map_equipement_nom);
        TextView infoAdresse = view.findViewById(R.id.map_equipement_adresse);
        TextView infoSite = view.findViewById(R.id.map_equipement_site);
        TextView infoTelephone = view.findViewById(R.id.map_equipement_telephone);
        View border = view.findViewById(R.id.map_equipement_border);

        // on split le snippet pour recuperer toutes les informations
        String[] infos = marker.getSnippet().split(getContext().getString(R.string.SEPARATEUR_SNIPPET));

        switch(infos[0]){
            // cas d'un equipement
            case TYPE_EQUIPEMENT :
                border.setVisibility(View.VISIBLE);

                // on recupere le nom de l'equipement
                titleTextView.setText(marker.getTitle());
                // en fonction de la longueur du nom, on adapte la taille de la police pour que ca tienne sur une ligne
                if(titleTextView.getText().length() > 34) {
                    titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, (680 / titleTextView.getText().length()));
                } else {
                    titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                }

                // on recupere l'adresse
                StringBuilder builder = new StringBuilder();
                builder.append(infos[1]);
                builder.append(", ");
                builder.append(infos[2]);
                builder.append(" ");
                builder.append(infos[3]);
                infoAdresse.setText(builder.toString());
                infoAdresse.setVisibility(View.VISIBLE);

                // on recupere le site web s'il existe
                if (infos[4].trim().isEmpty()) {
                    infoSite.setText(getContext().getString(R.string.site_inconnu));
                    infoSite.setTypeface(infoSite.getTypeface(), Typeface.ITALIC);
                } else {
                    infoSite.setText(infos[4]);
                }
                infoSite.setVisibility(View.VISIBLE);

                // on recupere le numéro de telephone s'il existe
                if (infos[5].trim().isEmpty()) {
                    infoTelephone.setText(getContext().getString(R.string.telephone_inconnu));
                    infoTelephone.setTypeface(infoTelephone.getTypeface(), Typeface.ITALIC);
                } else {
                    infoTelephone.setText(infos[5]);
                }
                infoTelephone.setVisibility(View.VISIBLE);
                break;
            case TYPE_PARKING :
                // cas du parking
                titleTextView.setText(marker.getTitle());
                // en fonction de la longueur du nom, on adapte la taille de la police pour que ca tienne sur une ligne
                if(titleTextView.getText().length() > 34) {
                    titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, (680 / titleTextView.getText().length()));
                } else {
                    titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                }
                infoAdresse.setVisibility(View.GONE);
                infoSite.setVisibility(View.GONE);
                infoTelephone.setVisibility(View.GONE);
                border.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        miseAJourDesInfos(marker, mWindow);
        return mWindow;
    }

    public View getWindow() {
        return mWindow;
    }

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context mContext) {
        this.mContext = mContext;
    }
}

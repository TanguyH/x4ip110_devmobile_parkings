package org.miage.parking.model.dao;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * La classe representant le premier niveau dans le parsing de l'appel REST aux Disponibilités
 */
public class Opendata {

    @Expose
    @SerializedName("request")
    public String request;

    @Expose
    @SerializedName("answer")
    public Answer answer;
}

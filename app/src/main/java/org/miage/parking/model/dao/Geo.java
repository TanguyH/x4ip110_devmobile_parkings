package org.miage.parking.model.dao;

import com.activeandroid.Model;
import com.google.gson.annotations.Expose;

/**
 * Classe Geo
 */
public class Geo extends Model{

    public Geo() {
        super();
    }

    @Expose
    public String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package org.miage.parking.model.dao;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

import java.util.ArrayList;

/**
 * Classe pour les moyens de paiement d'un Parking
 */
@Table(name="MoyenDePaiement")
public class MoyenDePaiement extends Model {

    public MoyenDePaiement() { super(); }

    @Expose
    @Column(name = "IdParking", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private int idParking;

    @Expose
    @Column(name = "IsCB")
    private boolean isCB;

    @Expose
    @Column(name = "IsEspeces")
    private boolean isEspeces;

    @Expose
    @Column(name = "IsCheque")
    private boolean isCheque;

    @Expose
    @Column(name = "IsTitreDeTransport")
    private boolean isTitreDeTransport;

    @Expose
    @Column(name = "IsTotalGR")
    private boolean isTotalGR;

    public void setAllAttributs(String str) {
        isCB = false;
        isEspeces= false;
        isCheque = false;
        isTotalGR = false;
        isTitreDeTransport = false;
        if (str != null){
            String[] moyens = str.split(",");
            for (String m: moyens) {
                switch (m) {
                    case "CB en borne de sortie":
                        isCB = true;
                        break;
                    case "Espèces":
                    case " Espèces":
                        isEspeces = true;
                        break;
                    case "chèque":
                    case " chèque":
                        isCheque = true;
                        break;
                    case " Total GR":
                    case "Total GR":
                        isTotalGR = true;
                        break;
                    case "Titre de transport":
                    case " Titre de transport":
                        isTitreDeTransport = true;
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public int getIdParking() {
        return idParking;
    }

    public void setIdParking(int idParking) {
        this.idParking = idParking;
    }

    public boolean isCB() {
        return isCB;
    }

    public void setCB(boolean cb) {
        isCB = cb;
    }

    public boolean isEspeces() {
        return isEspeces;
    }

    public void setEspeces(boolean especes) {
        isEspeces = especes;
    }

    public boolean isCheque() {
        return isCheque;
    }

    public void setCheque(boolean cheque) {
        isCheque = cheque;
    }

    public boolean isTotalGR() {
        return isTotalGR;
    }

    public void setTotalGR(boolean totalGR) {
        isTotalGR = totalGR;
    }

    public boolean isTitreDeTransport() {
        return isTitreDeTransport;
    }

    public void setTitreDeTransport(boolean titreDeTransport) {
        isTitreDeTransport = titreDeTransport;
    }

    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        ArrayList<String> tab = new ArrayList<>();

        // on recupere les moyens de paiement
        if(isCB) {
            tab.add("CB");
        }
        if(isCheque) {
            tab.add("Cheque");
        }
        if(isEspeces) {
            tab.add("Especes");
        }
        if(isTotalGR) {
            tab.add("Total GR");
        }
        if(isTitreDeTransport) {
            tab.add("Titre de Transport");
        }

        // s'il y en a, on construit la chaine de caractere qui correspond
        if(!tab.isEmpty()){
            for(int i = 0; i < tab.size() - 1; i++){
                builder.append(tab.get(i) + ", ");
            }
            builder.append(tab.get(tab.size()-1));
        } else {
            builder.append("Aucun moyen de paiement spécifié.");
        }

        return builder.toString();
    }
}

package org.miage.parking.model;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import org.miage.parking.R;
import org.miage.parking.model.dao.Parking;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Classe servant a mapper les informations des Parkings vers la View
 */
public class ParkingAdapter extends ArrayAdapter<Parking>{
    @BindView(R.id.parking_adapter_geo_name)
    TextView parkingGeoNameTextView;

    @BindView(R.id.parking_adapter_adresse)
    TextView parkingAdresseTextView;

    @BindView(R.id.parking_adapter_capacite)
    TextView parkingCapaciteTextView;

    @BindView(R.id.parking_adapter_credit_card)
    ImageView parkingCreditCardImageView;

    @BindView(R.id.parking_adapter_cash)
    ImageView parkingCashImageView;

    @BindView(R.id.parking_adapter_cheque)
    ImageView parkingChequeImageView;

    @BindView(R.id.parking_adapter_gr_card)
    ImageView parkingGrCardImageView;

    @BindView(R.id.parking_adapter_isFavoris)
    CheckBox mIsFavorite;

    @BindView(R.id.parking_adapter_icon_categorie)
    ImageView parkingIconCategorieView;

    @BindView(R.id.parking_adapter_parking_relais_moyen_de_paiement)
    TextView parkingMpParkingRelaisTextView;

    public ParkingAdapter(Context context, List<Parking> parkings){
        super(context, -1, parkings);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View actualView = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            actualView = inflater.inflate(R.layout.parking_adapter, parent, false);
        }
        ButterKnife.bind(this, actualView);
        Parking parking = getItem(position);

        // on recupere le nom
        parkingGeoNameTextView.setText(parking.geoName);

        // on recupere l'adresse
        parkingAdresseTextView.setText(parking.getAdresseTotal());

        // on recupere le statut "favoris" du parking et on l'empeche d'être cliquable
        mIsFavorite.setEnabled(false);

        // on recupere les informations liees a la disponibilite du parking
        if(parking.getDisponibilite() != null) {
            StringBuilder builder = new StringBuilder();
            // si l'identifiant est negatif
            if(parking.getDisponibilite().getGrpIdentifiant() < 0) {
                // c'est qu'on se trouve sur une disponibilite fictive (cas des parkings relais et des parkings sans disponibilite)
                // ce sont donc des disponibilites creees par l'application et sans les informations necessaires
                // on affiche donc que le nombre de place total qui est present sur le parking
                builder.append(getContext().getString(R.string.disponibilite_parking_relais));
                builder.append(" ");
                builder.append(parking.getCapaciteTotale());
                parkingCapaciteTextView.setText(builder.toString());
            }else {
                // sinon on affiche les informations reelles qu'on a
                builder.append(getContext().getString(R.string.disponibilite_parking));
                builder.append(" ");
                builder.append(parking.getDisponibilite().getGrpDisponible());
                builder.append(" / ");
                builder.append(parking.getCapaciteTotale());
                parkingCapaciteTextView.setText(builder.toString());
            }
            mIsFavorite.setChecked(parking.getDisponibilite().getIsFavoris());
        } else {
            parkingCapaciteTextView.setText(getContext().getString(R.string.disponibilite_non_chargee));
            mIsFavorite.setChecked(false);
        }

        // si c'est un parking relais
        if(parking.isParkingRelais()) {
            parkingIconCategorieView.setImageBitmap(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.ic_parking_relais_listview));
            // on recupere les infos des moyens de paiement
            if(parking.getMoyenDePaiement() != null && !parking.getMoyenDePaiement().toString().equals(getContext().getString(R.string.moyen_de_paiement_inconnu))){
                StringBuilder builderMoyen = new StringBuilder();
                builderMoyen.append(getContext().getString(R.string.details_moyen_de_paiement));
                builderMoyen.append(" : ");
                builderMoyen.append(parking.getMoyenDePaiement().toString());
                parkingMpParkingRelaisTextView.setText(builderMoyen);
            } else {
                parkingMpParkingRelaisTextView.setText(getContext().getString(R.string.moyen_de_paiement_inconnu));
            }

            parkingMpParkingRelaisTextView.setVisibility(View.VISIBLE);
            parkingCreditCardImageView.setVisibility(View.GONE);
            parkingCashImageView.setVisibility(View.GONE);
            parkingChequeImageView.setVisibility(View.GONE);
            parkingGrCardImageView.setVisibility(View.GONE);
        } else {
            // sinon, c'est un parking
            // on recupere les moyens de paiement
            if (parking.getMoyenDePaiement().isCB()) {
                parkingCreditCardImageView.setVisibility(View.VISIBLE);
            } else {
                parkingCreditCardImageView.setVisibility(View.INVISIBLE);
            }

            if (parking.getMoyenDePaiement().isEspeces()) {
                parkingCashImageView.setVisibility(View.VISIBLE);
            } else {
                parkingCashImageView.setVisibility(View.INVISIBLE);
            }

            if (parking.getMoyenDePaiement().isCheque()) {
                parkingChequeImageView.setVisibility(View.VISIBLE);
            } else {
                parkingChequeImageView.setVisibility(View.INVISIBLE);
            }

            if (parking.getMoyenDePaiement().isTotalGR()) {
                parkingGrCardImageView.setVisibility(View.VISIBLE);
            } else {
                parkingGrCardImageView.setVisibility(View.INVISIBLE);
            }

            parkingIconCategorieView.setImageBitmap(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.ic_parking_listview));
            parkingMpParkingRelaisTextView.setVisibility(View.GONE);
        }

        changementCouleur(actualView, position);

        return actualView;
    }

    public void changementCouleur(View actualView, int position){
        if(position%2==0){
            actualView.setBackgroundColor(Color.WHITE);
        } else{
            actualView.setBackgroundColor(Color.rgb(223,223,223));
        }
    }
}

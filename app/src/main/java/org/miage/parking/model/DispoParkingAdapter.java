package org.miage.parking.model;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.activeandroid.ActiveAndroid;
import org.miage.parking.R;
import org.miage.parking.model.dao.Disponibilite;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Classe servant a mapper les informations des Disponibilites vers la View
 */
public class DispoParkingAdapter extends ArrayAdapter<Disponibilite>{
    @BindView(R.id.details_parking_adapter_randomText)
    TextView dispoParkingRandomTextView;

    @BindView(R.id.details_parking_adapter_randomText_text)
    TextView dispoParkingRandomTextViewText;

    @BindView(R.id.details_parking_adapter_presentationText)
    TextView dispoParkingPresentationTextView;

    @BindView(R.id.details_parking_adapter_libType_text)
    TextView dispoParkingLibTypeTextViewText;

    @BindView(R.id.details_parking_adapter_libType)
    TextView dispoParkingLibTypeTextView;

    @BindView(R.id.details_parking_adapter_date)
    TextView dispoParkingDateTextView;

    @BindView(R.id.details_parking_adapter_nom)
    TextView dispoParkingNomTextView;

    @BindView(R.id.details_parking_adapter_dispo)
    TextView dispoParkingDispoTextView;

    @BindView(R.id.details_parking_adapter_dispo_text)
    TextView dispoParkingDispoTextViewText;

    @BindView(R.id.details_parking_adapter_exploitation)
    TextView dispoParkingExploitationTextView;

    @BindView(R.id.details_parking_adapter_exploitation_text)
    TextView dispoParkingExploitationTextViewText;

    @BindView(R.id.details_parking_adapter_statut)
    TextView dispoParkingStatutTextView;

    @BindView(R.id.details_parking_adapter_statut_text)
    TextView dispoParkingStatutTextViewText;

    @BindView(R.id.details_parking_adapter_priAuto)
    TextView dispoParkingPriAutoTextView;

    @BindView(R.id.details_parking_adapter_priAuto_text)
    TextView dispoParkingPriAutoTextViewText;

    @BindView(R.id.details_parking_adapter_switchText_text)
    TextView dispoParkingSwitchTextViewText;

    @BindView(R.id.details_parking_adapter_isFavorite)
    CheckBox mIsFavorite;

    @BindView(R.id.details_parking_adapter_bike)
    CheckBox mIsBike;

    @BindView(R.id.details_parking_adapter_moto)
    CheckBox mIsMoto;

    @BindView(R.id.details_parking_adapter_pmr)
    CheckBox mIsPMR;

    @BindView(R.id.details_parking_adapter_electricCar)
    CheckBox mIsElectricCar;

    Disponibilite dispo;

    public DispoParkingAdapter(Context context, List<Disponibilite> disponibilites){
        super(context, -1, disponibilites);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View actualView = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            actualView = inflater.inflate(R.layout.details_parking_adapter, parent, false);
        }
        ButterKnife.bind(this, actualView);
        dispo = getItem(position);

        // on recupere le nom
        dispoParkingNomTextView.setText(dispo.getGrpNom());
        // si le nom depasse les 20 caracteres (qui est la limite pour l'affichage avec un textsize de 32sp
        if(dispo.getGrpNom().length() > 20) {
            // on reduit le moins possible la textSize pour permettre le bon affichage
            // 640 etant la valeur max estimee en prenant en compte un nom de 20 caracteres en taille 32sp
            dispoParkingNomTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, (640/dispo.getGrpNom().length()));
        }
        dispoParkingRandomTextView.setText(String.valueOf(dispo.getGrpComplet()));

        dispoParkingRandomTextViewText.setText(getContext().getString(R.string.details_nb_places_mini_avant_complet));

        // on recupere la date de derniere mise à jour
        SpannableString date = new SpannableString(dispo.getGrpHorodatage());
        date.setSpan(new StyleSpan(Typeface.BOLD),0,date.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        SpannableStringBuilder builderDate = new SpannableStringBuilder();
        builderDate.append(getContext().getString(R.string.details_derniere_mise_a_jour) + " ");
        builderDate.append(date);
        dispoParkingDateTextView.setText(builderDate);

        // on recupere les differentes informations
        dispoParkingDispoTextView.setText(String.valueOf(dispo.getGrpDisponible()));
        dispoParkingDispoTextViewText.setText(getContext().getString(R.string.details_nb_places_dispo));
        dispoParkingExploitationTextView.setText(String.valueOf(dispo.getGrpExploitation()));
        dispoParkingExploitationTextViewText.setText(getContext().getString(R.string.details_nb_places_pour_clients_horaires));
            dispoParkingPriAutoTextView.setText(String.valueOf(dispo.getGrpPriAut()));
        dispoParkingPriAutoTextViewText.setText(getContext().getString(R.string.details_niveau_priorite_auto));
        dispoParkingSwitchTextViewText.setText(getContext().getString(R.string.details_favoris));
        switch(dispo.getGrpPriAut()){
            case 0 :
                dispoParkingStatutTextView.setText(getContext().getString(R.string.details_statut_neutre));
                break;
            case 1 :
                dispoParkingStatutTextView.setText(getContext().getString(R.string.details_statut_ferme));
                break;
            case 2 :
                dispoParkingStatutTextView.setText(getContext().getString(R.string.details_statut_abonnes));
                break;
            case 5 :
                dispoParkingStatutTextView.setText(getContext().getString(R.string.details_statut_complet));
                break;
            default:
                break;
        }
        dispoParkingStatutTextViewText.setText(getContext().getString(R.string.details_statut));

        //pas possible de cliquer sur les checkbox
        mIsBike.setEnabled(false);
        mIsMoto.setEnabled(false);
        mIsPMR.setEnabled(false);
        mIsElectricCar.setEnabled(false);

        dispoParkingLibTypeTextViewText.setText(getContext().getString(R.string.details_lib_type));

        //maj de la valeur des checkbox
        if (dispo.getParking() != null) {
            mIsBike.setChecked(dispo.getParking().isStationnementVelo());
            mIsMoto.setChecked(dispo.getParking().getCapaciteMoto() != null);
            mIsPMR.setChecked(dispo.getParking().isAccesPMR());
            mIsElectricCar.setChecked(dispo.getParking().getCapaciteVehiculeElectrique() != null);
            if(dispo.getParking().getPresentation() == null) {
                // s'il n'y a pas de presentation, on rend invisible le champ correspondant pour gagner en place
                dispoParkingPresentationTextView.setVisibility(View.GONE);
            } else if(dispo.getParking().getPresentation().equals(getContext().getString(R.string.disponibilite_inconnu))) {
                // si le champ n'est pas null, on regarde si c'est un cas particulier (parking CHU 2)
                dispoParkingPresentationTextView.setText(dispo.getParking().getPresentation());
                dispoParkingPresentationTextView.setTextColor(Color.RED);
                dispoParkingPresentationTextView.setTypeface(dispoParkingPresentationTextView.getTypeface(), Typeface.BOLD);
            } else {
                dispoParkingPresentationTextView.setText(dispo.getParking().getPresentation());
            }

            if(dispo.getParking().getLibType() != null) {
                switch (dispo.getParking().getLibType()) {
                    case "Souterrain":
                    case " Souterrain":
                        dispoParkingLibTypeTextView.setText(getContext().getString(R.string.details_lib_type_souterrain));
                        break;
                    case "Mixte":
                    case " Mixte":
                        dispoParkingLibTypeTextView.setText(getContext().getString(R.string.details_lib_type_mixte));
                        break;
                    case "Elévation":
                    case " Elévation":
                        dispoParkingLibTypeTextView.setText(getContext().getString(R.string.details_lib_type_elevation));
                        break;
                    default:
                        dispoParkingLibTypeTextView.setText(getContext().getString(R.string.valeur_non_dispo));
                        break;
                }
            } else {
                dispoParkingLibTypeTextView.setText(getContext().getString(R.string.valeur_non_dispo));
            }
        }

        mIsFavorite.setChecked(dispo.getIsFavoris());

        /* Regarder pour save l'update du Favoris*/
        mIsFavorite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                ActiveAndroid.beginTransaction();
                if ( isChecked ){
                    dispo.setIsFavoris(true);
                } else {
                    dispo.setIsFavoris(false);
                }

                dispo.save();
                dispo.getParking().save();

                ActiveAndroid.setTransactionSuccessful();
                ActiveAndroid.endTransaction();
            }
        });

        return actualView;
    }
}
package org.miage.parking.model;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import org.miage.parking.R;
import org.miage.parking.model.dao.EquipementCulturel;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Classe servant a mapper les informations des Equipements Culturels vers la View
 */
public class EquipementCulturelAdapter extends ArrayAdapter<EquipementCulturel>{
    @BindView(R.id.equipement_culturel_adapter_geo_name)
    TextView equipementCulturelGeoNameTextView;

    @BindView(R.id.equipement_culturel_adapter_adresse)
    TextView equipementCulturelAdresseTextView;

    @BindView(R.id.equipement_culturel_adapter_site)
    TextView equipementCulturelSiteTextView;

    @BindView(R.id.equipement_culturel_adapter_telephone)
    TextView equipementCulturelTelephoneTextView;

    @BindView(R.id.equipement_culturel_adapter_icon_categorie)
    ImageView equipementCulturelIconCategorieView;

    public EquipementCulturelAdapter(Context context, List<EquipementCulturel> equipements){
        super(context, -1, equipements);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View actualView = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            actualView = inflater.inflate(R.layout.equipement_culturel_adapter, parent, false);
        }
        ButterKnife.bind(this, actualView);
        EquipementCulturel equipement = getItem(position);

        // on recupere le nom
        equipementCulturelGeoNameTextView.setText(equipement.getGeoName());

        // en fonction de la longueur du nom, on adapte la taille de la police pour que ca tienne sur une ligne
        if(equipement.getGeoName().length() > 46) {
            equipementCulturelGeoNameTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, (725 / equipement.getGeoName().length()));
        } else {
            equipementCulturelGeoNameTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        }

        // on recupere l'adresse
        equipementCulturelAdresseTextView.setText(equipement.getAdresse()+", "+equipement.getCodePostal());

        // on recupere le site web s'il existe
        if(equipement.getWeb() != null){
            equipementCulturelSiteTextView.setText(equipement.getWeb());
        } else {
            equipementCulturelSiteTextView.setText(getContext().getString(R.string.site_inconnu));
            equipementCulturelSiteTextView.setTypeface(equipementCulturelSiteTextView.getTypeface(), Typeface.ITALIC);
        }

        // on recupere le numéro de téléphone s'il existe
        if(equipement.getTelephone() != null) {
            equipementCulturelTelephoneTextView.setText(equipement.getTelephone());
        } else {
            equipementCulturelTelephoneTextView.setText(getContext().getString(R.string.telephone_inconnu));
            equipementCulturelTelephoneTextView.setTypeface(equipementCulturelTelephoneTextView.getTypeface(), Typeface.ITALIC);
        }

        // on renseigne une icone en fonction de la catégorie de l'equipement
        switch (equipement.getCategorie()) {
            case 103 :
                equipementCulturelIconCategorieView.setImageBitmap(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.ic_equipement_mediatheque));
                break;
            case 101 :
                equipementCulturelIconCategorieView.setImageBitmap(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.ic_equipement_castle));
                break;
            case 108 :
                equipementCulturelIconCategorieView.setImageBitmap(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.ic_equipement_cinema));
                break;
            case 106 :
                equipementCulturelIconCategorieView.setImageBitmap(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.ic_equipement_art));
                break;
            case 107 :
                equipementCulturelIconCategorieView.setImageBitmap(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.ic_equipement_spectacle));
                break;
            case 104 :
                equipementCulturelIconCategorieView.setImageBitmap(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.ic_equipement_monument));
                break;
            case 105 :
                equipementCulturelIconCategorieView.setImageBitmap(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.ic_equipement_ecole));
                break;
            default:
                break;
        }

        if(position%2==0){
            actualView.setBackgroundColor(Color.WHITE);
        } else{
            actualView.setBackgroundColor(Color.rgb(223,223,223));
        }

        return actualView;
    }
}

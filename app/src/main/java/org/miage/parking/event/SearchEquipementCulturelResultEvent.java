package org.miage.parking.event;

import org.miage.parking.model.dao.EquipementCulturel;
import java.util.List;

/**
 * La classe representant l'evenement d'une recherche des Equipements Culturels
 */
public class SearchEquipementCulturelResultEvent {
    private List<EquipementCulturel> equipements;

    public SearchEquipementCulturelResultEvent(List<EquipementCulturel> equipements) {
        this.equipements = equipements;
    }

    // la liste des equipements
    public List<EquipementCulturel> getEquipementsCulturels() {
        return equipements;
    }
}

package org.miage.parking.event;

import org.miage.parking.model.dao.Disponibilite;
import java.util.List;

/**
 * La classe representant l'evenement d'une recherche des Disponibilités
 */
public class SearchDispoParkingResultEvent {
    private List<Disponibilite> disponibilites;

    public SearchDispoParkingResultEvent(List<Disponibilite> disponibilites) {
        this.disponibilites = disponibilites;
    }

    // la liste des disponibilites
    public List<Disponibilite> getDisponibilite() {
        return disponibilites;
    }
}

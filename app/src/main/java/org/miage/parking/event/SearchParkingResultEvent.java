package org.miage.parking.event;

import org.miage.parking.model.dao.Parking;
import java.util.List;

/**
 * La classe representant l'evenement d'une recherche des Parkings
 */
public class SearchParkingResultEvent {
    private List<Parking> parkings;

    public SearchParkingResultEvent(List<Parking> parkings) {
        this.parkings = parkings;
    }

    // la liste des parkings
    public List<Parking> getParkings() {
        return parkings;
    }
}

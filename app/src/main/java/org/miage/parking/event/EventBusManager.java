package org.miage.parking.event;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * La classe representant l'EventBusManager
 */
public class EventBusManager {

    // le bus
    public static Bus BUS = new Bus(ThreadEnforcer.ANY);
}

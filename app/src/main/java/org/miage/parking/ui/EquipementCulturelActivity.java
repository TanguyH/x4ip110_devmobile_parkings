package org.miage.parking.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import com.squareup.otto.Subscribe;
import org.miage.parking.R;
import org.miage.parking.event.EventBusManager;
import org.miage.parking.event.SearchEquipementCulturelResultEvent;
import org.miage.parking.model.dao.EquipementCulturel;
import org.miage.parking.model.EquipementCulturelAdapter;
import org.miage.parking.service.EquipementCulturelSearchService;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Classe pour l'activité des Equipements Culturels
 */
public class EquipementCulturelActivity extends Outils {
    @BindView(R.id.listEquipementCulturelView)
    ListView mListEquipementCulturelView;

    @BindView(R.id.activity_parking_search_parking_edittext)
    EditText mSearchEditText;

    @BindView(R.id.map_list_to_equipement_button)
    Button button;

    private EquipementCulturelAdapter equipementCulturel;
    private int idParking;
    private float latitude;
    private float longitude;
    private String nomParking;

    private List<EquipementCulturel> equipementsEnCache = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.equipement_parking_main);

        // Binding ButterKnife annotations now that content view has been set
        ButterKnife.bind(this);

        Intent i = getIntent();
        idParking = i.getIntExtra("idParking", 0);
        longitude = i.getFloatExtra("longitudeParking",0);
        latitude = i.getFloatExtra("latitudeParking",0);
        nomParking = i.getStringExtra("nomParking");

        // Instanciance EquipementCulturelAdapter with empty content
        equipementCulturel = new EquipementCulturelAdapter(this, new ArrayList<EquipementCulturel>());
        mListEquipementCulturelView.setAdapter(equipementCulturel);

        mSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // Nothing to do when texte is about to change
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // While text is changing, hide list and show loader
            }

            @Override
            public void afterTextChanged(Editable editable) {
                equipementCulturel.clear();
                List<EquipementCulturel> equipAAfficher = new ArrayList<>();
                // on effectue les recherches sur les equipements en cache
                // on se sert d'une liste en cache pour ne pas avoir a refaire des appels en base et lancer d'appel REST
                for(EquipementCulturel e : equipementsEnCache) {
                    if(e.getGeoName().contains(editable.toString()) || e.getAdresse().contains(editable.toString()) || e.getLibCategorie().equals(editable.toString())) {
                        equipAAfficher.add(e);
                    }
                }
                equipementCulturel.addAll(equipAAfficher);
            }
        });

        // au clique sur le bouton, on passe sur une googlemap qui affichera les equipements culturels et le parking de reference
        // on passe directement les equipements a l'intent en les transformant en Parcelable
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent mapEquipementIntent = new Intent(EquipementCulturelActivity.this, MapEquipementCulturelActivity.class);
                mapEquipementIntent.putExtra("idParking", idParking);
                mapEquipementIntent.putExtra("latitudeParking", latitude);
                mapEquipementIntent.putExtra("longitudeParking", longitude);
                mapEquipementIntent.putExtra("nomParking", nomParking);
                mapEquipementIntent.putExtra("nbEquipementsEnCache", equipementsEnCache.size());
                for(int i = 0; i < equipementsEnCache.size(); i++) {
                    EquipementCulturel newE = new EquipementCulturel(equipementsEnCache.get(i).getIdObj(), equipementsEnCache.get(i).getGeoName(),equipementsEnCache.get(i).getAdresse(),equipementsEnCache.get(i).getLatitude(),equipementsEnCache.get(i).getLongitude(),equipementsEnCache.get(i).getLibCategorie(),equipementsEnCache.get(i).getLibTheme(),equipementsEnCache.get(i).getStatut(),equipementsEnCache.get(i).getTelephone(),equipementsEnCache.get(i).getCommune(),equipementsEnCache.get(i).getCodePostal(),equipementsEnCache.get(i).getWeb(),equipementsEnCache.get(i).getLibType());
                    mapEquipementIntent.putExtra("equipement_"+i, newE);
                }
                startActivity(mapEquipementIntent);
            }
        });
    }

    @Override
    protected void onResume() {
        // Do NOT forget to call super.onResume()
        super.onResume();

        // Register to Event bus : now each time an event is posted, the activity will receive it if it is @Subscribed to this event
        EventBusManager.BUS.register(this);

        SharedPreferences sharedPref = getBaseContext().getSharedPreferences(getResources().getString(R.string.PREFS), Context.MODE_PRIVATE);
        double rayon = sharedPref.getFloat(getResources().getString(R.string.PREFS_RAYON), 400);

        // Refresh search
        EquipementCulturelSearchService.INSTANCE.searchEquipementCulturelFromDB(latitude, longitude, rayon);
    }

    @Override
    protected void onPause() {
        // Unregister from Event bus : if event are posted now, the activity will not receive it
        EventBusManager.BUS.unregister(this);

        // Do NOT forget to call super.onPause()
        super.onPause();
    }

    @Subscribe
    public void searchResult(final SearchEquipementCulturelResultEvent event) {
        // Here someone has posted a SearchResultEvent
        // Run on ui thread as we want to update UI
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                equipementCulturel.clear();
                equipementsEnCache.clear();
                equipementsEnCache.addAll(event.getEquipementsCulturels());
                equipementCulturel.addAll(event.getEquipementsCulturels());
            }
        });
    }
}

package org.miage.parking.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.otto.Subscribe;
import org.miage.parking.R;
import org.miage.parking.event.EventBusManager;
import org.miage.parking.event.SearchParkingResultEvent;
import org.miage.parking.model.CustomInfoWindowMapParkingAdapter;
import org.miage.parking.model.dao.EquipementCulturel;
import org.miage.parking.model.dao.MoyenDePaiement;
import org.miage.parking.model.dao.Parking;
import org.miage.parking.service.EquipementCulturelSearchService;
import org.miage.parking.service.ParkingSearchService;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Classe correspond a l'activite MapParkingActivity
 */
public class MapParkingActivity extends Outils implements OnMapReadyCallback {

    @BindView(R.id.map_parking_activity_search_parking_edittext)
    EditText mMapParkingSearchEditText;

    @BindView(R.id.map_parking_activity_loader)
    ProgressBar mMapParkingProgressBar;

    // la map
    private GoogleMap mActiveGoogleMap;

    // la liste des parkings
    private ArrayList<Parking> listParkings = new ArrayList<>();
    //La liste des parkings affichés
    ArrayList<Parking> listParkingAffiches = new ArrayList<>();
    // la liste des equipements
    private ArrayList<EquipementCulturel> listEquipements = new ArrayList<>();

    // une map reliant le marker et le parking
    private Map<String, Parking> mMarkersToParking = new LinkedHashMap<>();
    // une map reliant le marker et l'equipement
    private Map<String, EquipementCulturel> mMarkersToEquipement = new LinkedHashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_main);

        // Binding ButterKnife annotations now that content view has been set
        ButterKnife.bind(this);

        // definition et setting du composant affichant la map
        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.map_parking_activity_mapview);
        mapFragment.getMapAsync(this);

        // ajout du listener pour le filtre
        mMapParkingSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //Ne fait rien
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //Ne fait rien
            }

            @Override
            public void afterTextChanged(Editable editable) {
                // affichage du loader
                mMapParkingProgressBar.setVisibility(View.VISIBLE);

                // Lancement de la recherche des Parkings
                ParkingSearchService.INSTANCE.searchParkingFromDBWithScheduler(editable.toString(), editable.toString(), null, null, null, null, null, null, isModeAvionOn(MapParkingActivity.this), null, null, afficherParkingRelais(), false);
            }
        });
    }

    @Override
    protected void onResume() {
        // appel a la methode du parent
        super.onResume();

        // abonnement au bus
        EventBusManager.BUS.register(this);

        // appel au service REST pour recuperer les parkings
        ParkingSearchService.INSTANCE.searchParkingFromDBWithScheduler(mMapParkingSearchEditText.getText().toString(), mMapParkingSearchEditText.getText().toString(), null, null, null, null, null, null,  isModeAvionOn(MapParkingActivity.this), null, null, afficherParkingRelais(), true);
    }

    @Override
    protected void onPause() {
        // Unregister from Event bus : if event are posted now, the activity will not receive it
        EventBusManager.BUS.unregister(this);

        // Do NOT forget to call super.onPause()
        super.onPause();
    }

    @Subscribe
    public void searchResult(final SearchParkingResultEvent event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                listParkings.clear();

                // on recupere la liste des parkings
                listParkings.addAll(event.getParkings());

                // on met a jour la map
                updateMap();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mActiveGoogleMap = map;
    }

    /**
     * Permet de mettre a jour la map, on faisant la configuration, en ajoutant les markers et en centrant la camera.
     */
    private void updateMap(){
        // si la map est ok
        if(mActiveGoogleMap != null){
            // on configure la map (liste, adapter, actionListener)
            configurationDeLaMap();

            // si dans les preferences, on souhaite afficher les equipements culturels
            SharedPreferences sharedPreferences = getBaseContext().getSharedPreferences("PREFS",Context.MODE_PRIVATE);
            //getResources().getString(R.string.SHOW_EQUIP)
            if(sharedPreferences.getBoolean(getResources().getString(R.string.SHOW_EQUIP), false)) {
                //on recupere les equipements
                recupererEquipementsCulturels();

                // pour chaque equipement
                for(EquipementCulturel e : listEquipements){
                    ajoutMarker(e);
                }
            }

            Boolean preferenceParkingRelais = sharedPreferences.getBoolean(getResources().getString(R.string.SHOW_PARKING_RELAIS), false);
            // pour chaque parking
            for(Parking p : listParkings){
                //On affiche tous les parkings et les parkings relais si la préference est setté à true
                if(!p.isParkingRelais() || (p.isParkingRelais() && preferenceParkingRelais)){
                    ajoutMarker(p);
                    listParkingAffiches.add(p);
                }
            }

            // si on a au moins un parking
            if(!listParkingAffiches.isEmpty()){
                deplacementDeLaCamera(preferenceParkingRelais);
            }

            // on cache le loarder
            mMapParkingProgressBar.setVisibility(View.GONE);
        }
    }

    /**
     * Permet de reinitialiser la configuration de la map (markers, adapter, listener).
     */
    private void configurationDeLaMap() {
        // on clear la map
        mActiveGoogleMap.clear();
        mMarkersToParking.clear();
        mMarkersToEquipement.clear();

        // on definit l'adapter custom pour l'infoWindow
        CustomInfoWindowMapParkingAdapter customAdapter = new CustomInfoWindowMapParkingAdapter(MapParkingActivity.this);
        mActiveGoogleMap.setInfoWindowAdapter(customAdapter);

        // on ajoute l'action du click sur les markers
        mActiveGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                // on recupere le parking associe au marker qui a declenche l'event
                Parking parking = mMarkersToParking.get(marker.getId());
                if (parking != null) {
                    Intent intentParkingDetail;
                    // on definit l'intent pour passer sur la page des details
                    if(parking.isParkingRelais()) {
                        intentParkingDetail = new Intent(MapParkingActivity.this, ParkingRelaisDetailActivity.class);
                    } else {
                        intentParkingDetail = new Intent(MapParkingActivity.this, ParkingDetailActivity.class);
                    }
                    intentParkingDetail.putExtra("idParking", parking.getIdObj());
                    intentParkingDetail.putExtra("longitudeParking", parking.longitude);
                    intentParkingDetail.putExtra("latitudeParking", parking.latitude);
                    intentParkingDetail.putExtra("nomParking", parking.geoName);
                    startActivity(intentParkingDetail);
                }
            }
        });
    }

    /**
     * Permet de recuperer la liste des equipements culturels en base.
     */
    private void recupererEquipementsCulturels() {
        listEquipements.addAll(EquipementCulturelSearchService.INSTANCE.recupererEquipementCulturelFromDB());
    }
    /**
     * Permet d'ajouter un marker pour le Parking passe en parametre.
     * @param p le parking qui correspond au marker qui sera ajoute a la map
     */
    private void ajoutMarker(Parking p) {
        if(p != null) {
            // on recupere ses informations
            LatLng latLng = new LatLng(p.getLatitude(), p.getLongitude());

            //on definit les options du marker
            MarkerOptions markerOptions = new MarkerOptions()
                    .position(latLng)
                    .title(p.getGeoName())
                    .snippet(generateSnippetForInfoWindow(p));

            //Si parking relai, on le mets en bleu
            if(p.isParkingRelais()){
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
            }

            // on ajoute un marker
            Marker marker = mActiveGoogleMap.addMarker(markerOptions);

            // et on stock l'id du marker et son parking associe
            mMarkersToParking.put(marker.getId(), p);
        }
    }

    /**
     * Permet d'ajouter un marker pour l'Equipement passe en parametre.
     * @param e l'equipement qui correspond au marker qui sera ajoute a la map
     */
    private void ajoutMarker(EquipementCulturel e) {
        if(e != null) {
            // on recupere ses informations
            LatLng latLng = new LatLng(e.getLatitude(), e.getLongitude());

            //on definit les options du marker
            MarkerOptions markerOptions = new MarkerOptions()
                    .position(latLng)
                    .title(e.getGeoName())
                    .snippet(generateSnippetForInfoWindow(e))
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

            // on ajoute un marker
            Marker marker = mActiveGoogleMap.addMarker(markerOptions);

            // et on stock l'id du marker et son equipement associe
            mMarkersToEquipement.put(marker.getId(), e);
        }
    }

    /**
     * Permet de regrouper les informations d'un Parking sous la forme d'une chaine de caractere.
     * @param p le parking dont on veut concatener les informations
     * @return une chaine de caractere correspondant aux informations du Parking passe en parametre
     */
    private String generateSnippetForInfoWindow(Parking p){
        // creation du separateur
        String separator = getResources().getString(R.string.SEPARATEUR_SNIPPET);
        StringBuilder builder = new StringBuilder();
        // le type
        builder.append(getResources().getString(R.string.TYPE_PARKING));
        builder.append(separator);
        // adresse
        builder.append(p.getAdresse());
        builder.append(separator);

        if(!p.isParkingRelais()) {
            // si les infos de la Disponibilite sont chargees, on les affiche
            // sinon on affiche un message de non disponibilite
            builder.append(MapParkingActivity.this.getString(R.string.map_nb_places_dispo));
            if (p.getDisponibilite() != null) {
                builder.append(" " + p.getDisponibilite().getGrpDisponible() + " / " + p.getCapaciteTotale());
                builder.append(separator);
                builder.append(p.getDisponibilite().getIsFavoris());
                builder.append(separator);
            } else {
                builder.append(" " + MapParkingActivity.this.getString(R.string.valeur_non_dispo) + " / " + p.getCapaciteTotale());
                builder.append(separator);
                builder.append(false);
                builder.append(separator);
            }
        } else {
            builder.append(MapParkingActivity.this.getString(R.string.map_nb_places_total));
            builder.append(" " + p.getCapaciteTotale());
            builder.append(separator);
            builder.append(p.getDisponibilite().getIsFavoris());
            builder.append(separator);
        }

        // moyen de paiement
        MoyenDePaiement paiement = p.getMoyenDePaiement();
        builder.append(paiement.toString());
        return builder.toString();
    }

    /**
     * Permet de regrouper les informations d'un equipement sous la forme d'une chaine de caractere.
     * @param e l'equipement dont on veut concatener les informations
     * @return une chaine de caractere correspondant aux informations du Equipement passe en parametre
     */
    private String generateSnippetForInfoWindow(EquipementCulturel e){
        // creation du separateur
        String separator = getResources().getString(R.string.SEPARATEUR_SNIPPET);
        StringBuilder builder = new StringBuilder();
        // le type
        builder.append(getResources().getString(R.string.TYPE_EQUIPEMENT));
        builder.append(separator);
        // adresse
        builder.append(e.getAdresse());
        builder.append(separator);
        // commune
        builder.append(e.getCommune());
        builder.append(separator);
        // code postal
        builder.append(e.getCodePostal());
        builder.append(separator);
        // site
        if(e.getWeb() != null) {
            builder.append(e.getWeb());
        }else {
            builder.append(" ");
        }
        builder.append(separator);
        // telephone
        if(e.getTelephone() != null) {
            builder.append(e.getTelephone());
        }else {
            builder.append(" ");
        }
        builder.append(separator);
        return builder.toString();
    }

    /**
     * Permet de centrer la camera sur les coordonnees globales des Parkings.
     */
    private void deplacementDeLaCamera(Boolean affichageParkingRelais) {
        // on veut centrer la camera sur les parkings
        int nbParkings = listParkingAffiches.size();
        double latGlobale = 0d;
        double lonGlobale = 0d;
        for(Parking p : listParkingAffiches){
            latGlobale += p.getLatitude();
            lonGlobale += p.getLongitude();
        }
        // on recupere les coordonnees des parkings et on centre sur la moyenne
        LatLng latLngNantes = new LatLng( latGlobale/nbParkings, lonGlobale/nbParkings);
        mActiveGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngNantes, 13));
        // animation de la camera
        if(affichageParkingRelais){
            mActiveGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(11), 2000, null);
        } else {
            mActiveGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(13), 2000, null);
        }
    }

}

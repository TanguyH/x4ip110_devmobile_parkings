package org.miage.parking.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import org.miage.parking.R;
import org.miage.parking.event.EventBusManager;
import org.miage.parking.event.SearchParkingResultEvent;
import org.miage.parking.model.ParkingAdapter;
import org.miage.parking.model.dao.Parking;
import org.miage.parking.service.ParkingSearchService;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

/**
 * Classe pour l'activité RechercheParkingActivity qui permet de rechercher des parkings selon
 * certains critères
 */
public class RechercheParkingActivity extends Outils  {
    @BindView(R.id.listView)
    ListView mListView;

    @BindView(R.id.recherche_parking_adresseText)
    EditText mAdresseEditText;

    @BindView(R.id.recherche_parking_nomText)
    EditText mNomEditText;

    @BindView(R.id.recherche_parking_cbCheckbox)
    CheckBox mCbCheckBox;

    @BindView(R.id.recherche_parking_chequeCheckbox)
    CheckBox mChequeCheckBox;

    @BindView(R.id.recherche_parking_especeCheckbox)
    CheckBox mEspeceCheckBox;

    @BindView(R.id.recherche_parking_totalGRCheckbox)
    CheckBox mTotalGRCheckBox;

    @BindView(R.id.recherche_parking_seekBarNbPlace)
    SeekBar mNbPlaceSeekBar;

    @BindView(R.id.recherche_parking_textSeekBar)
    TextView mTextSeekBarTextView;

    @BindView(R.id.recherche_parking_rechercheBouton)
    Button mRechercheButton;

    @BindView(R.id.activity_parking_loader)
    ProgressBar mProgressBar;

    private ParkingAdapter placeParking;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recherche_parking_main);

        // Binding ButterKnife annotations now that content view has been set
        ButterKnife.bind(this);

        mNbPlaceSeekBar.setProgress(0);
        mNbPlaceSeekBar.setMax(400);
        mNbPlaceSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //Ne fait rien
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //Ne fait rien
            }
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                int val = (progress * (seekBar.getWidth() - 2 * seekBar.getThumbOffset())) / seekBar.getMax();
                mTextSeekBarTextView.setText("" + progress);
                //textView.setY(100); just added a value set this properly using screen with height aspect ratio , if you do not set it by default it will be there below seek bar
            }
        });
        // Instanciance ParkingAdapter with empty content
        placeParking = new ParkingAdapter(this, new ArrayList<Parking>());
        mListView.setAdapter(placeParking);

        // Récupération des préférences de recherche de l'utilisateur
        sharedPreferences = getBaseContext().getSharedPreferences(getResources().getString(R.string.PREFS), MODE_PRIVATE);

        if (sharedPreferences.getBoolean(getResources().getString(R.string.PREFS_CB),false)){
            mCbCheckBox.setChecked(true);
        }
        if (sharedPreferences.getBoolean(getResources().getString(R.string.PREFS_ESPECE),false))   {
            mEspeceCheckBox.setChecked(true);
        }
        if (sharedPreferences.getBoolean(getResources().getString(R.string.PREFS_CHEQUE),false))   {
            mChequeCheckBox.setChecked(true);
        }
        if (sharedPreferences.getBoolean(getResources().getString(R.string.PREFS_GR),false)) {
            mTotalGRCheckBox.setChecked(true);
        }
        if (sharedPreferences.getInt(getResources().getString(R.string.PREFS_MINPLACES),0) > 0) {
            mNbPlaceSeekBar.setProgress(sharedPreferences.getInt(getResources().getString(R.string.PREFS_MINPLACES), 0));
            mTextSeekBarTextView.setText("" + mNbPlaceSeekBar.getProgress());
        }

        mRechercheButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mProgressBar.setVisibility(View.VISIBLE);
                    // Launch a search through the PlaceSearchService
                    ParkingSearchService.INSTANCE.searchParkingFromDBWithScheduler(mNomEditText.getText().toString(),mAdresseEditText.getText().toString(), mCbCheckBox.isChecked(), mChequeCheckBox.isChecked(),
                            mEspeceCheckBox.isChecked(), mTotalGRCheckBox.isChecked(), null, mNbPlaceSeekBar.getProgress(),isModeAvionOn(RechercheParkingActivity.this), true, true, afficherParkingRelais(), false);
                }

         });
    }

    @OnItemClick(R.id.listView)
    public void onItemSelected(int position) {
        Intent parkingDetailIntent = new Intent(RechercheParkingActivity.this, ParkingDetailActivity.class);
        Parking place = placeParking.getItem(position);
        parkingDetailIntent.putExtra("idParking", place.getIdObj());
        parkingDetailIntent.putExtra("longitudeParking", place.longitude);
        parkingDetailIntent.putExtra("latitudeParking", place.latitude);
        parkingDetailIntent.putExtra("nomParking", place.geoName);
        startActivity(parkingDetailIntent);
    }

    @Override
    protected void onResume() {
        // Do NOT forget to call super.onResume()
        super.onResume();

        // Register to Event bus : now each time an event is posted, the activity will receive it if it is @Subscribed to this event
        EventBusManager.BUS.register(this);
    }

    @Override
    protected void onPause() {
        // Unregister from Event bus : if event are posted now, the activity will not receive it
        EventBusManager.BUS.unregister(this);

        // Do NOT forget to call super.onPause()
        super.onPause();
    }

    @Subscribe
    public void searchResult(final SearchParkingResultEvent event) {
        // Here someone has posted a SearchResultEvent
        // Run on ui thread as we want to update UI
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Step 1: Update adapter's model
                placeParking.clear();
                placeParking.addAll(event.getParkings());
                // Step 2: hide loader
                mProgressBar.setVisibility(View.GONE);
            }
        });
    }
}

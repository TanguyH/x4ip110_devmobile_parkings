package org.miage.parking.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.miage.parking.R;
import org.miage.parking.service.DispoParkingSearchService;
import org.miage.parking.service.ParkingRelaisSearchService;
import org.miage.parking.service.ParkingSearchService;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Classe pour l'activité ParamActivity qui gere les reglages de l'application
 */
public class ParamActivity extends Outils  {
    @BindView(R.id.parametre_parking_cbCheckbox)
    CheckBox mCBCheckbox;

    @BindView(R.id.parametre_especeCheckbox)
    CheckBox mEspeceCheckbox;

    @BindView(R.id.parametre_parking_chequeCheckbox)
    CheckBox mChequeCheckbox;

    @BindView(R.id.parametre_GRCheckbox)
    CheckBox mGRCheckbox;

    @BindView(R.id.parametre_parking_seekBarNbPlace)
    SeekBar mNbPlaceSeekBar;

    @BindView(R.id.parametre_parking_textSeekBar)
    TextView mTextSeekBarTextView;

    @BindView(R.id.parametre_fenetre_principale)
    Spinner mFenetrePrincSpinner;

    @BindView(R.id.parametre_show_equipement_switch)
    Switch mShowEquipementSwitch;

    @BindView(R.id.parametre_show_parking_relais_switch)
    Switch mShowParkingRelaiSwitch;

    @BindView(R.id.parametre_show_parking_relais_recherche_switch)
    Switch mShowParkingRelaiRechercheSwitch;

    @BindView(R.id.parametre_reset_favoris)
    Button mResetFavorisButton;

    @BindView(R.id.parametre_refresh_parking)
    Button mResetParkingButton;

    @BindView(R.id.parametre_refresh_parking_relais)
    Button mResetParkingRelaisButton;

    @BindView(R.id.param_rayon_value)
    EditText mEditTextParamRayonValue;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.parameter);// Binding ButterKnife annotations now that content view has been set
        ButterKnife.bind(this);

        mNbPlaceSeekBar.setProgress(0);
        mNbPlaceSeekBar.setMax(400);

        List<String> nomsFenetres = new ArrayList();
        nomsFenetres.add(getResources().getString(R.string.menu_accueil));
        nomsFenetres.add(getResources().getString(R.string.menu_listeParkings));
        nomsFenetres.add(getResources().getString(R.string.menu_carte));
        nomsFenetres.add(getResources().getString(R.string.menu_favoris));
        nomsFenetres.add(getResources().getString(R.string.menu_recherche));

        //Choix de l'activité par défaut
        ArrayAdapter adapterNomsFenetres = new ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item,
                nomsFenetres
        );
        adapterNomsFenetres.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mFenetrePrincSpinner.setAdapter(adapterNomsFenetres);

        sharedPreferences = getBaseContext().getSharedPreferences(getResources().getString(R.string.PREFS), MODE_PRIVATE);

        //Récupération des choix fait par l'utilisateur sur les différents critères
        if (sharedPreferences.getBoolean(getResources().getString(R.string.PREFS_CB),false)) {
            mCBCheckbox.setChecked(true);
        }

        if (sharedPreferences.getBoolean(getResources().getString(R.string.PREFS_ESPECE),false)) {
            mEspeceCheckbox.setChecked(true);
        }
        if (sharedPreferences.getBoolean(getResources().getString(R.string.PREFS_CHEQUE),false)) {
            mChequeCheckbox.setChecked(true);
        }
        if (sharedPreferences.getBoolean(getResources().getString(R.string.PREFS_GR),false)) {
            mGRCheckbox.setChecked(true);
        }
        if (sharedPreferences.getInt(getResources().getString(R.string.PREFS_MINPLACES),0) > 0) {
            mNbPlaceSeekBar.setProgress(sharedPreferences.getInt(getResources().getString(R.string.PREFS_MINPLACES), 0));
            mTextSeekBarTextView.setText("" + mNbPlaceSeekBar.getProgress());
        }
        if (sharedPreferences.getInt(getResources().getString(R.string.PREFS_FENPRINC),0) > 0) {
            mFenetrePrincSpinner.setSelection(sharedPreferences.getInt(getResources().getString(R.string.PREFS_FENPRINC),0));
        }
        if (sharedPreferences.getBoolean(getResources().getString(R.string.SHOW_EQUIP),false)) {
            mShowEquipementSwitch.setChecked(true);
        }
        if(sharedPreferences.getBoolean(getResources().getString(R.string.SHOW_PARKING_RELAIS), false)){
            mShowParkingRelaiSwitch.setChecked(true);
        }
        if(sharedPreferences.getBoolean(getResources().getString(R.string.SHOW_PARKING_RELAIS_RECHERCHE), false)){
            mShowParkingRelaiRechercheSwitch.setChecked(true);
        }
        int rayon = (int) sharedPreferences.getFloat(getResources().getString(R.string.PREFS_RAYON), 400f);
        mEditTextParamRayonValue.setText(Integer.toString(rayon));

        // Préférences de recherches sur les différents critères
        mCBCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            sharedPreferences
                    .edit()
                    .putBoolean(getResources().getString(R.string.PREFS_CB), mCBCheckbox.isChecked())
                    .apply();
            }

        });
        mEspeceCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferences
                        .edit()
                        .putBoolean(getResources().getString(R.string.PREFS_ESPECE), mEspeceCheckbox.isChecked())
                        .apply();
            }

        });
        mChequeCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferences
                        .edit()
                        .putBoolean(getResources().getString(R.string.PREFS_CHEQUE), mChequeCheckbox.isChecked())
                        .apply();
            }

        });
        mGRCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferences
                        .edit()
                        .putBoolean(getResources().getString(R.string.PREFS_GR), mGRCheckbox.isChecked())
                        .apply();
            }

        });
        mNbPlaceSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { // Ne fait rien
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                sharedPreferences
                        .edit()
                        .putInt(getResources().getString(R.string.PREFS_MINPLACES), mNbPlaceSeekBar.getProgress())
                        .apply();
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                int val = (progress * (seekBar.getWidth() - 2 * seekBar.getThumbOffset())) / seekBar.getMax();
                mTextSeekBarTextView.setText("" + progress);
                // mTextSeekBarTextView.setX(seekBar.getX() + val + seekBar.getThumbOffset() / 2.0f);
                //textView.setY(100); just added a value set this properly using screen with height aspect ratio , if you do not set it by default it will be there below seek bar
            }
        });

        // Gestion de la fenêtre principale
        mFenetrePrincSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                sharedPreferences
                        .edit()
                        .putInt(getResources().getString(R.string.PREFS_FENPRINC), i)
                        .apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //Ne fait rien
            }
        });
        // au clique sur le bouton, on supprime tous les favoris de l'utilisateur
        mResetFavorisButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DispoParkingSearchService.INSTANCE.deleteFavorite();
                Toast.makeText(ParamActivity.this, R.string.param_delete_favoris, Toast.LENGTH_LONG).show();
            }

        });

        // au clique sur le bouton, on refresh la bdd des parkings
        mResetParkingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParkingSearchService.INSTANCE.searchParkingFromAddress(isModeAvionOn(ParamActivity.this));
                Toast.makeText(ParamActivity.this, R.string.param_refresh_parking, Toast.LENGTH_LONG).show();
            }
        });

        // au clique sur le bouton, on refresh la bdd des parkings relais
        mResetParkingRelaisButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParkingRelaisSearchService.INSTANCE.searchParkingFromAddress(isModeAvionOn(ParamActivity.this));
                Toast.makeText(ParamActivity.this, R.string.param_refresh_parking_relais, Toast.LENGTH_LONG).show();
            }
        });

        // preference pour afficher ou non les equipements culturels sur la map
        mShowEquipementSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferences
                        .edit()
                        .putBoolean(getResources().getString(R.string.SHOW_EQUIP), mShowEquipementSwitch.isChecked())
                        .apply();
            }
        });

        // preference pour afficher ou non les parking relais sur la map
        mShowParkingRelaiSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferences
                        .edit()
                        .putBoolean(getResources().getString(R.string.SHOW_PARKING_RELAIS), mShowParkingRelaiSwitch.isChecked())
                        .apply();
            }
        });

        // preference pour afficher ou non les parking relais dans les recherches
        mShowParkingRelaiRechercheSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferences
                        .edit()
                        .putBoolean(getResources().getString(R.string.SHOW_PARKING_RELAIS_RECHERCHE), mShowParkingRelaiRechercheSwitch.isChecked())
                        .apply();
            }
        });
        mEditTextParamRayonValue.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                Float value;
                if (mEditTextParamRayonValue.getText().toString().isEmpty()) {
                    value = Float.parseFloat("400");
                } else {
                    value = Float.parseFloat(mEditTextParamRayonValue.getText().toString());
                }
                sharedPreferences
                        .edit()
                        .putFloat(getResources().getString(R.string.PREFS_RAYON), value)
                        .apply();
            }
        });
    }
}

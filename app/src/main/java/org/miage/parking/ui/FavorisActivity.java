package org.miage.parking.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.squareup.otto.Subscribe;

import org.miage.parking.R;
import org.miage.parking.event.EventBusManager;
import org.miage.parking.event.SearchParkingResultEvent;
import org.miage.parking.model.ParkingAdapter;
import org.miage.parking.model.dao.Parking;
import org.miage.parking.service.ParkingSearchService;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

/**
 * Classe pour l'activité des Equipements Culturels
 */
public class FavorisActivity  extends Outils {
    @BindView(R.id.listView)
    ListView mListView;

    @BindView(R.id.activity_parking_search_parking_edittext)
    EditText mSearchEditText;

    @BindView(R.id.activity_parking_loader)
    ProgressBar mProgressBar;

    private ParkingAdapter placeParking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.parking_main);

        // Binding ButterKnife annotations now that content view has been set
        ButterKnife.bind(this);

        // Instanciance ParkingAdapter with empty content
        placeParking = new ParkingAdapter(this, new ArrayList<Parking>());
        mListView.setAdapter(placeParking);

        // Set textfield value according to intent
        if (getIntent().hasExtra("currentSearch")) {
            mSearchEditText.setText(getIntent().getStringExtra("currentSearch"));
        }

        mSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // Nothing to do when texte is about to change
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // While text is changing, hide list and show loader
            }

            @Override
            public void afterTextChanged(Editable editable) {
                // Once text has changed
                // Show a loader
                mProgressBar.setVisibility(View.VISIBLE);

                // Launch a search through the PlaceSearchService
                ParkingSearchService.INSTANCE.searchParkingFromDBWithScheduler(editable.toString(), editable.toString(), null, null, null, null, true, null,  isModeAvionOn(FavorisActivity.this), null, null, afficherParkingRelais(), false);
            }
        });
    }

    @OnItemClick(R.id.listView)
    public void onItemSelected(int position) {
        Intent parkingDetailIntent = new Intent(FavorisActivity.this, ParkingDetailActivity.class);
        Parking place = placeParking.getItem(position);
        parkingDetailIntent.putExtra("idParking", place.getIdObj());
        parkingDetailIntent.putExtra("longitudeParking", place.longitude);
        parkingDetailIntent.putExtra("latitudeParking", place.latitude);
        parkingDetailIntent.putExtra("nomParking", place.geoName);
        startActivity(parkingDetailIntent);
    }

    @Override
    protected void onResume() {
        // Do NOT forget to call super.onResume()
        super.onResume();

        // Register to Event bus : now each time an event is posted, the activity will receive it if it is @Subscribed to this event
        EventBusManager.BUS.register(this);

        // Refresh search
        ParkingSearchService.INSTANCE.searchParkingFromDBWithScheduler(mSearchEditText.getText().toString(), mSearchEditText.getText().toString(), null, null, null, null, true, null, isModeAvionOn(FavorisActivity.this), null,null, afficherParkingRelais(), true);
    }

    @Override
    protected void onPause() {
        // Unregister from Event bus : if event are posted now, the activity will not receive it
        EventBusManager.BUS.unregister(this);

        // Do NOT forget to call super.onPause()
        super.onPause();
    }

    @Subscribe
    public void searchResult(final SearchParkingResultEvent event) {
        // Here someone has posted a SearchResultEvent
        // Run on ui thread as we want to update UI
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Step 1: Update adapter's model
                placeParking.clear();
                placeParking.addAll(event.getParkings());
                // Step 2: hide loader
                mProgressBar.setVisibility(View.GONE);
            }
        });
    }
}

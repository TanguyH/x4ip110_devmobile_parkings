package org.miage.parking.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import org.miage.parking.R;

/**
 * Classe generale pour les activités
 */
public class Outils extends AppCompatActivity implements MenuItem.OnMenuItemClickListener, NavigationView.OnNavigationItemSelectedListener{
    private FrameLayout viewStub; //This is the framelayout to keep your content view
    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.app_base_layout);// The base layout that contains your navigation drawer.

        viewStub = findViewById(R.id.view_stub);
        NavigationView navigationView = findViewById(R.id.navigation_view); // The new navigation view from Android Design Library. Can inflate menu resources.
        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, 0, 0);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        Menu drawerMenu = navigationView.getMenu();
        for(int i = 0; i < drawerMenu.size(); i++) {
            drawerMenu.getItem(i).setOnMenuItemClickListener(this);
        }
        // and so on...
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /* Override all setContentView methods to put the content view to the FrameLayout viewStub
     * so that, we can make other activity implementations looks like normal activity subclasses.
     */
    @Override
    public void setContentView(int layoutResID) {
        if (viewStub != null) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            View stubView = inflater.inflate(layoutResID, viewStub, false);
            viewStub.addView(stubView, lp);
        }
    }

    @Override
    public void setContentView(View view) {
        if (viewStub != null) {
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            viewStub.addView(view, lp);
        }
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        if (viewStub != null) {
            viewStub.addView(view, params);
        }
    }

    /*
        Methode permettant de définir un autre NavigationView pour des activités particulières
        comme la MainActivity
     */
    public void setContentViewSpecialActivity(int id, boolean isSpecialActivity){
        // Si c'est une activité particulière (MainActivity, etc)
        // on execute la méthode de la classe mère AppCompatActivity
        if(isSpecialActivity) {
            super.setContentView(id);
        } else {
            // Sinon on utilise la méthode que l'on a override
            setContentView(id);
        }
    }

    public void setContentViewSpecialActivity(View view, boolean isSpecialActivity){
        if(isSpecialActivity) {
            super.setContentView(view);
        } else {
            setContentView(view);
        }
    }

    public void setContentViewSpecialActivity(View view, ViewGroup.LayoutParams params, boolean isSpecialActivity){
        if(isSpecialActivity) {
            super.setContentView(view, params);
        } else {
            setContentView(view, params);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.boutonAccueil:
                Intent parkingAccueil = new Intent(this, MainActivity.class);
                startActivity(parkingAccueil);
                break;
            case R.id.boutonParking:
                Intent parkingIntent = new Intent(this, ParkingActivity.class);
                startActivity(parkingIntent);
                break;
            case R.id.boutonCarte:
                Intent carteIntent = new Intent(this, MapParkingActivity.class);
                startActivity(carteIntent);
                break;
            case R.id.boutonRecherche:
                Intent rechercheIntent = new Intent(this, RechercheParkingActivity.class);
                startActivity(rechercheIntent);
                break;
            case R.id.boutonFavoris:
                Intent favorisIntent = new Intent(this, FavorisActivity.class);
                startActivity(favorisIntent);
                break;
            case R.id.boutonReglage:
                Intent paramIntent = new Intent(this, ParamActivity.class);
                startActivity(paramIntent);
                break;
            default: break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()) {
            case R.id.boutonAccueil:
                Intent parkingAccueil = new Intent(this, MainActivity.class);
                startActivity(parkingAccueil);
                break;
            case R.id.boutonParking:
                Intent parkingIntent = new Intent(this, ParkingActivity.class);
                startActivity(parkingIntent);
                break;
            case R.id.boutonCarte:
                Intent carteIntent = new Intent(this, MapParkingActivity.class);
                startActivity(carteIntent);
                break;
            case R.id.boutonRecherche:
                Intent rechercheIntent = new Intent(this, RechercheParkingActivity.class);
                startActivity(rechercheIntent);
                break;
            case R.id.boutonFavoris:
                Intent favorisIntent = new Intent(this, FavorisActivity.class);
                startActivity(favorisIntent);
                break;
            case R.id.boutonReglage:
                Intent paramIntent = new Intent(this, ParamActivity.class);
                startActivity(paramIntent);
                break;
            default: break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
    * Permet de savoir si le mode Avion (HL) est active
    * @param context le context (activite) qui fera l'appel pour obtenir l'information
    * @return un boolean, true si le mode Avion est active, false sinon
    */
    protected boolean isModeAvionOn(android.content.Context context) {
        return android.provider.Settings.Global.getInt(context.getContentResolver(), android.provider.Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }

    /**
     * Permet de savoir si on souhaite afficher les parkings relais ou non selon les préferences dans les réglages
     * @return un boolean, true si on souhaite afficher les parkings relais, false sinon
     */
    protected boolean afficherParkingRelais() {
        SharedPreferences sharedPreferences = getBaseContext().getSharedPreferences(getResources().getString(R.string.PREFS), Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(getResources().getString(R.string.SHOW_PARKING_RELAIS_RECHERCHE), false);
    }
}

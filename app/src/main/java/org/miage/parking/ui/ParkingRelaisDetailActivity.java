package org.miage.parking.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.otto.Subscribe;

import org.miage.parking.R;
import org.miage.parking.event.EventBusManager;
import org.miage.parking.event.SearchDispoParkingResultEvent;
import org.miage.parking.model.DispoParkingRelaisAdapter;
import org.miage.parking.model.dao.Disponibilite;
import org.miage.parking.service.DispoParkingSearchService;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Classe pour l'activité ParkingRelaisDetailActivity qui affiche les details(dispo) d'un parking relais
 */
public class ParkingRelaisDetailActivity extends Outils implements OnStreetViewPanoramaReadyCallback {

    @BindView(R.id.listParkingDetailsView)
    ListView mListParkingDetailsView;

    @BindView(R.id.details_parking_adapter_button)
    Button button;

    private DispoParkingRelaisAdapter dispoParking;
    private int idParking;
    private float latitude;
    private float longitude;
    private String nomParking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_parking_main);

        // Binding ButterKnife annotations now that content view has been set
        ButterKnife.bind(this);

        Intent i = getIntent();
        idParking = i.getIntExtra("idParking", 0);
        longitude = i.getFloatExtra("longitudeParking",0);
        latitude = i.getFloatExtra("latitudeParking",0);
        nomParking = i.getStringExtra("nomParking");

        // Instanciance ParkingAdapter with empty content
        dispoParking = new DispoParkingRelaisAdapter(this, new ArrayList<Disponibilite>());
        mListParkingDetailsView.setAdapter(dispoParking);

        StreetViewPanoramaFragment streetViewPanoramaFragment =
                (StreetViewPanoramaFragment) getFragmentManager()
                        .findFragmentById(R.id.streetviewpanorama);
        streetViewPanoramaFragment.getStreetViewPanoramaAsync(this);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent equipementIntent = new Intent(ParkingRelaisDetailActivity.this, EquipementCulturelActivity.class);
                equipementIntent.putExtra("idParking", idParking);
                equipementIntent.putExtra("latitudeParking", latitude);
                equipementIntent.putExtra("longitudeParking", longitude);
                equipementIntent.putExtra("nomParking", nomParking);
                startActivity(equipementIntent);
            }
        });
    }

    @Override
    protected void onPause() {
        // Unregister from Event bus : if event are posted now, the activity will not receive it
        EventBusManager.BUS.unregister(this);

        // Do NOT forget to call super.onPause()
        super.onPause();
    }


    @Override
    protected void onResume() {
        // Do NOT forget to call super.onResume()
        super.onResume();

        // Register to Event bus : now each time an event is posted, the activity will receive it if it is @Subscribed to this event
        EventBusManager.BUS.register(this);

        // Refresh search
        DispoParkingSearchService.INSTANCE.searchDispoParkingFromDB(idParking);
    }

    @Subscribe
    public void searchResult(final SearchDispoParkingResultEvent event) {
        // Here someone has posted a SearchResultEvent
        // Run on ui thread as we want to update UI
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // on vide la liste des dispos
                dispoParking.clear();

                // si la liste des dispo recue contient plus d'un element
                // ca veut dire que c'est un appel lance par la recherche des parkings
                // quoi qu'il arrive, on ne veut afficher les infos que du parking qui nous interesse
                if(event.getDisponibilite().size() > 1) {
                    List<Disponibilite> disposRecup = event.getDisponibilite();
                    // on parcourt les dispos pour trouver celle qui correspond au bon parking
                    for(Disponibilite d : disposRecup) {
                        if(d.getParking() != null && d.getParking().getIdObj() == idParking) {
                            dispoParking.add(d);
                            break;
                        }
                    }
                } else {
                    dispoParking.addAll(event.getDisponibilite());
                }
            }
        });
    }

    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama panorama) {
        panorama.setPosition(new LatLng(latitude, longitude));
    }

}
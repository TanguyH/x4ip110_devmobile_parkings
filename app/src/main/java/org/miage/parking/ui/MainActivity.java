package org.miage.parking.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import org.miage.parking.R;
import org.miage.parking.model.CaseMenu;
import org.miage.parking.model.MainActivityAdapter;
import org.miage.parking.service.EquipementCulturelSearchService;
import org.miage.parking.service.ParkingRelaisSearchService;
import org.miage.parking.service.ParkingSearchService;
import org.miage.parking.service.DispoParkingSearchService;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Classe pour la page d'accueil
 */
public class MainActivity extends Outils {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    private static final String PREFS = "PREFS";
    private static final String PREFS_FENPRINC = "PREFS_FENPRINC";
    SharedPreferences sharedPreferences;

    //Case du menu principal
    private CaseMenu[] menuCases = {
            new CaseMenu(R.string.menu_listeParkings, R.drawable.ic_parking),
            new CaseMenu(R.string.menu_carte, R.drawable.ic_map),
            new CaseMenu(R.string.menu_favoris, R.drawable.ic_is_favoris),
            new CaseMenu(R.string.menu_recherche , R.drawable.ic_search)
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentViewSpecialActivity(R.layout.activity_main, true);

        // Binding ButterKnife annotations now that content view has been set
        ButterKnife.bind(this);

        sharedPreferences = getBaseContext().getSharedPreferences(PREFS, MODE_PRIVATE);

        Intent intent;
        //Choix de l'activité par défaut (paramètrable dans les réglages)
        if (sharedPreferences.contains(PREFS_FENPRINC) && sharedPreferences.getInt(PREFS_FENPRINC,0) > 0) {
            switch (sharedPreferences.getInt(PREFS_FENPRINC,0)){
                case 1:
                    intent = new Intent(MainActivity.this, ParkingActivity.class);
                    startActivity(intent);
                    break;
                case 2:
                    intent = new Intent(MainActivity.this, MapParkingActivity.class);
                    startActivity(intent);
                    break;
                case 3:
                    intent = new Intent(MainActivity.this, FavorisActivity.class);
                    startActivity(intent);
                    break;
                case 4:
                    intent = new Intent(MainActivity.this, RechercheParkingActivity.class);
                    startActivity(intent);
                    break;
                default: break;
            }
        }

        //Barre bleu située en haut de l'application <- Permet de mettre du text / couleur
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.syncState();
        drawer.addDrawerListener(toggle);

        //Menu contextuel
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Menu de la page d'accueil
        GridView gridView = findViewById(R.id.gridview);
        final MainActivityAdapter mainActivityAdapter = new MainActivityAdapter(this, menuCases);
        gridView.setAdapter(mainActivityAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        //Liste des parkings
                        Intent parkingIntent = new Intent(MainActivity.this, ParkingActivity.class);
                        startActivity(parkingIntent);
                        break;
                    case 1:
                        //Carte
                        Intent mapParkingIntent = new Intent(MainActivity.this, MapParkingActivity.class);
                        startActivity(mapParkingIntent);
                        break;
                    case 2:
                        //Favoris
                        Intent favorisIntent = new Intent(MainActivity.this, FavorisActivity.class);
                        startActivity(favorisIntent);
                        break;
                    case 3:
                        //Recherche de parkings
                        Intent rechercheIntent = new Intent(MainActivity.this, RechercheParkingActivity.class);
                        startActivity(rechercheIntent);
                        break;
                    default: break;
                }
            }
        });

        // au premier lancement de l'application
        // on lance 1 appel pour recuperer les parkings, les parkings relais et les équipements culturels
        // on le fait que la toute premiere fois pour optimiser le chargement
        // ce sont des objets qui n'ont pas de données variables et on a peu d'interet de les mettre à jour
        SharedPreferences sharedPref = getBaseContext().getSharedPreferences(getResources().getString(R.string.PREFS), Context.MODE_PRIVATE);
        if(sharedPref.getBoolean(getResources().getString(R.string.PREMIER_LANCEMENT), true)) {
            // si c'est le premier lancement, on fait un appel REST pour recuperer les infos
            ParkingSearchService.INSTANCE.searchParkingFromAddress(isModeAvionOn(MainActivity.this));
            DispoParkingSearchService.INSTANCE.searchDispoParkingFromAddress(null, isModeAvionOn(MainActivity.this));
            EquipementCulturelSearchService.INSTANCE.searchEquipementCulturelFromAddress(isModeAvionOn(MainActivity.this));
            ParkingRelaisSearchService.INSTANCE.searchParkingFromAddress(isModeAvionOn(MainActivity.this));
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putBoolean(getResources().getString(R.string.PREMIER_LANCEMENT), false);
            editor.commit();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}

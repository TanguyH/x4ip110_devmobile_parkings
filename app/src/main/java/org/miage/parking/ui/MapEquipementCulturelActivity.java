package org.miage.parking.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.miage.parking.R;
import org.miage.parking.model.CustomInfoWindowMapEquipementAdapter;
import org.miage.parking.model.dao.EquipementCulturel;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Classe correspond a l'activite MapEquipementCulturelActivity
 */
public class MapEquipementCulturelActivity extends Outils implements OnMapReadyCallback {

    @BindView(R.id.map_equipement_activity_search_edittext)
    EditText mMapEquipementSearchEditText;

    @BindView(R.id.map_equipement_to_list_button)
    Button button;

    // la map
    private GoogleMap mActiveGoogleMap;

    // la liste des parkings
    private ArrayList<EquipementCulturel> listeEquipements = new ArrayList<>();

    // une map reliant le marker et le parking
    private Map<String, EquipementCulturel> mMarkersToParking = new LinkedHashMap<>();
    private List<EquipementCulturel> equipementsEnCache = new ArrayList<>();
    private float latitude;
    private float longitude;
    private String nomParking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_equipement_main);

        // Binding ButterKnife annotations now that content view has been set
        ButterKnife.bind(this);

        Intent i = getIntent();
        longitude = i.getFloatExtra("longitudeParking",0);
        latitude = i.getFloatExtra("latitudeParking",0);
        nomParking = i.getStringExtra("nomParking");
        int nbEquipementsEnCache = i.getIntExtra("nbEquipementsEnCache", 0);

        // on recupere tous les equipements culturels
        for(int j = 0; j < nbEquipementsEnCache; j++){
            EquipementCulturel newE = getIntent().getExtras().getParcelable("equipement_"+j);
            equipementsEnCache.add(newE);
        }

        listeEquipements.addAll(equipementsEnCache);

        // definition et setting du composant affichant la map
        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.map_equipement_activity_mapview);
        mapFragment.getMapAsync(this);

        // ajout du listener pour le filtre
        mMapEquipementSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //Ne fait rien
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //Ne fait rien
            }

            @Override
            public void afterTextChanged(Editable editable) {
                listeEquipements.clear();
                for(EquipementCulturel e : equipementsEnCache) {
                    if(e.getGeoName().contains(editable.toString()) || e.getAdresse().contains(editable.toString()) || e.getLibCategorie().equals(editable.toString())) {
                        listeEquipements.add(e);
                    }
                }
                updateMap();
            }
        });

        // au clique sur le bouton, on repasse sur les equipements sous forme de liste
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getIntent().setClass(MapEquipementCulturelActivity.this, EquipementCulturelActivity.class);
                startActivity(getIntent());
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mActiveGoogleMap = map;
        updateMap();
    }

    /**
     * Permet de mettre a jour la map, on faisant la configuration, en ajoutant les markers et en centrant la camera.
     */
    private void updateMap(){
        // si la map est ok
        if(mActiveGoogleMap != null){
            // on configure la map (liste, adapter, actionListener)
            configurationDeLaMap();
            definirMarkerCustom();
            ajoutMarkerParking();

            // pour chaque parking
            for(EquipementCulturel e : listeEquipements){
                ajoutMarkerEquipement(e);
            }

            // on centre sur le parking
            deplacementDeLaCamera();
        }
    }

    /**
     * Permet de reinitialiser la configuration de la map (markers, adapter, listener).
     */
    private void configurationDeLaMap() {
        // on clear la map
        mActiveGoogleMap.clear();
        mMarkersToParking.clear();
    }

    /**
     * Permet de définir un marker custom pour les equipements
     */
    private void definirMarkerCustom() {
        // on definit l'adapter custom pour l'infoWindow
        CustomInfoWindowMapEquipementAdapter customAdapter = new CustomInfoWindowMapEquipementAdapter(MapEquipementCulturelActivity.this);
        mActiveGoogleMap.setInfoWindowAdapter(customAdapter);
    }

    /**
     * Permet d'ajouter un marker pour le Equipement passe en parametre.
     * @param e l"equipement qui correspond au marker qui sera ajoute a la map
     */
    private void ajoutMarkerEquipement(EquipementCulturel e) {
        if(e != null) {
            // on recupere ses informations
            LatLng latLng = new LatLng(e.getLatitude(), e.getLongitude());

            //on definit les options du marker
            MarkerOptions markerOptions = new MarkerOptions()
                    .position(latLng)
                    .title(e.getGeoName())
                    .snippet(generateSnippetForInfoWindow(e))
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

            // on ajoute un marker
            Marker marker = mActiveGoogleMap.addMarker(markerOptions);

            // et on stock l'id du marker et son equipement associe
            mMarkersToParking.put(marker.getId(), e);
        }
    }

    /**
     * Permet d'ajouter un marker pour le Parking a partir duquel on a lance l'ecran.
     */
    private void ajoutMarkerParking() {
        // on recupere ses informations
        LatLng latLng = new LatLng(latitude, longitude);

        StringBuilder builder = new StringBuilder();
        builder.append(getResources().getString(R.string.TYPE_PARKING));
        builder.append(getResources().getString(R.string.SEPARATEUR_SNIPPET));

        //on definit les options du marker
        MarkerOptions markerOptions = new MarkerOptions()
                .position(latLng)
                .title(nomParking)
                .snippet(builder.toString())
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));

        // on ajoute un marker
        mActiveGoogleMap.addMarker(markerOptions);
    }

    /**
     * Permet de regrouper les informations d'un equipement sous la forme d'une chaine de caractere.
     * @param e l'equipement dont on veut concatener les informations
     * @return une chaine de caractere correspondant aux informations du Equipement passe en parametre
     */
    private String generateSnippetForInfoWindow(EquipementCulturel e){
        // creation du separateur
        String separator = getResources().getString(R.string.SEPARATEUR_SNIPPET);
        StringBuilder builder = new StringBuilder();
        builder.append(getResources().getString(R.string.TYPE_EQUIPEMENT));
        builder.append(separator);
        // adresse
        builder.append(e.getAdresse());
        builder.append(separator);
        // commune
        builder.append(e.getCommune());
        builder.append(separator);
        // code postal
        builder.append(e.getCodePostal());
        builder.append(separator);
        // site
        if(e.getWeb() != null) {
            builder.append(e.getWeb());
        }else {
            builder.append(" ");
        }
        builder.append(separator);
        // telephone
        if(e.getTelephone() != null) {
            builder.append(e.getTelephone());
        }else {
            builder.append(" ");
        }
        builder.append(separator);
        return builder.toString();
    }

    /**
     * Permet de centrer la camera sur les coordonnees du parking.
     */
    private void deplacementDeLaCamera() {
        // on veut centrer la camera sur le parking
        LatLng latLngNantes = new LatLng( latitude, longitude);
        mActiveGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngNantes, 15));
        // animation de la camera
        mActiveGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
    }
}

package org.miage.parking.service;

import android.util.Log;
import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.miage.parking.event.EventBusManager;
import org.miage.parking.event.SearchEquipementCulturelResultEvent;
import org.miage.parking.model.CalculZoneDeProximite;
import org.miage.parking.model.dao.EquipementCulturel;
import org.miage.parking.model.EquipementCulturelSearchResult;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Classe pour les appels REST pour la localication des equipements culturels
 */
public class EquipementCulturelSearchService {
    private static final long REFRESH_DELAY = 650;
    public static final EquipementCulturelSearchService INSTANCE = new EquipementCulturelSearchService();
    private final EquipementCulturelSearchRESTService EquipementCulturelSearchRESTService;
    private ScheduledExecutorService mScheduler = Executors.newScheduledThreadPool(1);
    private ScheduledFuture mLastScheduleTask;

    // Constructor
    private EquipementCulturelSearchService() {
        // Create GSON Converter that will be used to convert from JSON to Java
        Gson gsonConverter = new GsonBuilder()
                .setLenient()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .excludeFieldsWithoutExposeAnnotation().create();

        // Create Retrofit client
        Retrofit retrofit = new Retrofit.Builder()
                // Using OkHttp as HTTP Client
                .client(new OkHttpClient())
                // Having the following as server URL
                .baseUrl("https://data.nantes.fr")
                // Using GSON to convert from Json to Java
                .addConverterFactory(GsonConverterFactory.create(gsonConverter))
                .build();

        // Use retrofit to generate our REST service code
        EquipementCulturelSearchRESTService = retrofit.create(EquipementCulturelSearchRESTService.class);
    }

    /**
     * Permet de faire un appel en base pour les EquipementsCulturels et d'envoyer le resultat sur le bus.
     * Les EquipementsCulturels recuperes dependent de la chaine de caractere passee en parametre.
     * Permet egalement de mettre a jour en base tous les EquipementsCulturels..
     * @param isModeAvionOn boolean indiquant si l'utilisateur est en mode Avion (HL)
     */
    public void searchEquipementCulturelFromAddress(final boolean isModeAvionOn) {
        // Cancel last scheduled network call (if any)
        if (mLastScheduleTask != null && !mLastScheduleTask.isDone()) {
            mLastScheduleTask.cancel(true);
        }

        // Schedule a network call in REFRESH_DELAY ms
        mLastScheduleTask = mScheduler.schedule(new Runnable() {
            public void run() {
                // Step 1 : first run a local search from DB and post result
                searchEquipementCulturelFromDB();

                // on regarde si on est en mode avion (HL), si c'est le cas pas besoin de faire l'appel
                if(!isModeAvionOn) {
                    // Step 2 : Call to the REST service
                    try {
                        EquipementCulturelSearchRESTService.searchForEquipementCulturel("json").enqueue(new Callback<EquipementCulturelSearchResult>() {
                            @Override
                            public void onResponse(Call<EquipementCulturelSearchResult> call, Response<EquipementCulturelSearchResult> response) {
                                // Post an event so that listening activities can update their UI
                                if (response.body() != null && response.body().data != null) {
                                    // Save all results in Database
                                    ActiveAndroid.beginTransaction();
                                    for (EquipementCulturel equipement : response.body().data) {
                                        equipement.getGeo().save();
                                        equipement.setGeoName(equipement.getGeo().getName());
                                        equipement.setLatitude(equipement.getCoordonnees().get(0));
                                        equipement.setLongitude(equipement.getCoordonnees().get(1));
                                        equipement.save();
                                    }
                                    ActiveAndroid.setTransactionSuccessful();
                                    ActiveAndroid.endTransaction();

                                    // Send a new event with results from network
                                    searchEquipementCulturelFromDB();
                                } else {
                                    // Null result
                                    // We may want to display a warning to user (e.g. Toast)

                                    Log.e("[EqpmentSrchRes] [REST]", "Response error : null body");
                                }
                            }

                            @Override
                            public void onFailure(Call<EquipementCulturelSearchResult> call, Throwable t) {
                                // Request has failed or is not at expected format
                                // We may want to display a warning to user (e.g. Toast)
                                Log.e("[EqpmentSrchRes] [REST]", "Response error : " + t.getMessage());
                            }
                        });
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                }
            }
        }, REFRESH_DELAY, TimeUnit.MILLISECONDS);
    }

    /**
     * Permet de faire une recherche en base pour recuperer tous les equipements dans une zone autour d'un point.
     */
    public void searchEquipementCulturelFromDB(double latitude, double longitude, double rayon) {
        // on recupere la zone de proximite dans laquelle on souhaite recuperer les equipements
        CalculZoneDeProximite zone = new CalculZoneDeProximite(latitude, longitude, rayon);

        // on cherche en base les equipements se trouvant dans la zone définie
        List<EquipementCulturel> matchingEquipementCulturelFromDB = new Select()
                .from(EquipementCulturel.class)
                .where("(Latitude < "+zone.getMaxLatitude()+" AND Latitude > "+zone.getMinLatitude()+") AND (Longitude < "+zone.getMaxLongitude()+" AND Longitude > "+zone.getMinLongitude()+")")
                .execute();

        // Post result as an event
        EventBusManager.BUS.post(new SearchEquipementCulturelResultEvent(matchingEquipementCulturelFromDB));
    }

    /**
     * Permet de faire une recherche en base pour recuperer tous les equipements.
     */
    public void searchEquipementCulturelFromDB() {
        // on cherche en base les equipements se trouvant dans la zone définie
        List<EquipementCulturel> matchingEquipementCulturelFromDB = new Select()
                .from(EquipementCulturel.class)
                .execute();

        // Post result as an event
        EventBusManager.BUS.post(new SearchEquipementCulturelResultEvent(matchingEquipementCulturelFromDB));
    }

    /**
     * Permet de recuperer en base tous les equipements.
     */
    public List<EquipementCulturel> recupererEquipementCulturelFromDB() {
        // on cherche en base les equipements se trouvant dans la zone définie
        List<EquipementCulturel> matchingEquipementCulturelFromDB = new Select()
                .from(EquipementCulturel.class)
                .execute();
        return matchingEquipementCulturelFromDB;
    }

    // Service describing the REST APIs
    public interface EquipementCulturelSearchRESTService {
        @GET("/api/publication/24440040400129_NM_NM_00019/LOC_EQUIPUB_CULTURE_NM_STBL/content/")
        Call<EquipementCulturelSearchResult> searchForEquipementCulturel(@Query("format") String format);
    }
}

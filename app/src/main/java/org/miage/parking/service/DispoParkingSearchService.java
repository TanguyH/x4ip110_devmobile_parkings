package org.miage.parking.service;

import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.miage.parking.event.EventBusManager;
import org.miage.parking.event.SearchDispoParkingResultEvent;
import org.miage.parking.model.dao.Disponibilite;
import org.miage.parking.model.dao.Parking;
import org.miage.parking.model.dao.Opendata;
import org.miage.parking.model.DispoParkingSearchResult;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * Classe pour les appels REST pour les Disponibilites
 */
public class DispoParkingSearchService {
    // delai pour le scheduler pour les appels a l'API
    private static final long REFRESH_DELAY = 650;
    public static final DispoParkingSearchService INSTANCE = new DispoParkingSearchService();
    private final DispoParkingSearchRESTService ParkingSearchRESTService;
    private ScheduledExecutorService mScheduler = Executors.newScheduledThreadPool(1);
    private ScheduledFuture mLastScheduleTask;

    // Constructor
    private DispoParkingSearchService() {
        // Create GSON Converter that will be used to convert from JSON to Java
        Gson gsonConverter = new GsonBuilder()
                .setLenient()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .excludeFieldsWithoutExposeAnnotation().create();

        // Create Retrofit client
        Retrofit retrofit = new Retrofit.Builder()
                // Using OkHttp as HTTP Client
                .client(new OkHttpClient())
                // Having the following as server URL
                .baseUrl("https://data.nantes.fr")
                // Using GSON to convert from Json to Java
                .addConverterFactory(GsonConverterFactory.create(gsonConverter))
                .build();

        // Use retrofit to generate our REST service code
        ParkingSearchRESTService = retrofit.create(DispoParkingSearchRESTService.class);
    }

    /**
     * Permet de faire un appel en base pour une Disponibilite et d'envoyer le resultat sur le bus.
     * La Disponibilite recuperee depend de l'id passe en parametre.
     * Permet egalement de mettre a jour en base toutes les Disponibilites.
     * @param idObj l'id qui servira a la recherche de la Disponibilite
     * @param isModeAvionOn boolean indiquant si l'utilisateur est en mode Avion (HL)
     */
    public void searchDispoParkingFromAddress(final int idObj, final boolean isModeAvionOn) {
        ArrayList<Integer> listeIds = new ArrayList<>();
        listeIds.add(idObj);
        searchDispoParkingFromAddress(listeIds, isModeAvionOn);
    }

    /**
     * Permet de reinitialiser en base le flag Favoris de tous les parkings
     */
    public void deleteFavorite(){
        List<Disponibilite> matchingDispoFromDB = new Select()
                .from(Disponibilite.class)
                .where("Favoris = 1")
                .execute();

        for (Disponibilite dispo : matchingDispoFromDB){
            dispo.setIsFavoris(false);
            dispo.save();
        }
    }

    /**
     * Permet de faire un appel en base pour les Disponibilites et d'envoyer le resultat sur le bus.
     * Les Disponibilites recuperees dependent de la liste des ids passee en parametre.
     * Permet egalement de mettre a jour en base toutes les Disponibilites.
     * @param IdsObj la liste des ids qui servira a la recherche des Disponibilites
     * @param isModeAvionOn boolean indiquant si l'utilisateur est en mode Avion (HL)
     */
    public void searchDispoParkingFromAddress(final ArrayList<Integer> IdsObj, final boolean isModeAvionOn) {
        final ArrayList<Integer>  idsObjSave = IdsObj;
        // Cancel last scheduled network call (if any)
        if (mLastScheduleTask != null && !mLastScheduleTask.isDone()) {
            mLastScheduleTask.cancel(true);
        }

        // Schedule a network call in REFRESH_DELAY ms
        mLastScheduleTask = mScheduler.schedule(new Runnable() {
            public void run() {
                // Step 1 : first run a local search from DB and post result
                searchDispoParkingFromDB(idsObjSave);

                // on regarde si on est en mode avion (HL), si c'est le cas pas besoin de faire l'appels
                if(!isModeAvionOn) {
                    // Step 2 : Call to the REST service
                    try {
                        ParkingSearchRESTService.searchForDispoParking().enqueue(new Callback<DispoParkingSearchResult>() {
                            @Override
                            public void onResponse(Call<DispoParkingSearchResult> call, Response<DispoParkingSearchResult> response) {
                                // Post an event so that listening activities can update their UI
                                if (response.body() != null && response.body().opendata != null) {
                                    // Save all results in Database
                                    ActiveAndroid.beginTransaction();
                                    Opendata dispo = response.body().opendata;
                                    for (Disponibilite dispo2 : dispo.answer.data.groupesParking.groupeParking){
                                        // on recupere le parking ayant le meme idObj
                                        Parking matchingParkingFromDB = new Select()
                                                .from(Parking.class)
                                                .where("IdObj =" + dispo2.getIdObj())
                                                .executeSingle();
                                        // s'il existe on le set a la disponibilite et inversement
                                        if(matchingParkingFromDB != null) {
                                            dispo2.setParking(matchingParkingFromDB);
                                            dispo2.getParking().setDisponibilite(dispo2);
                                            dispo2.getParking().save();
                                        }

                                        // on regarde si une disponibilite pour le meme idObj existe en base
                                        Disponibilite matchingDispoFromDB = new Select()
                                                .from(Disponibilite.class)
                                                .where("IdObj =" + dispo2.getIdObj())
                                                .executeSingle();
                                        //Si la dispo existait déjà, on récupère le favoris
                                        if (matchingDispoFromDB != null){
                                            dispo2.setIsFavoris(matchingDispoFromDB.getIsFavoris());
                                        } else {
                                            //Sinon un parking n'est pas en favoris par défaut
                                            dispo2.setIsFavoris(false);
                                        }

                                        dispo2.save();
                                    }
                                    ActiveAndroid.setTransactionSuccessful();
                                    ActiveAndroid.endTransaction();

                                    // Send a new event with results from network
                                    searchDispoParkingFromDB(idsObjSave);
                                } else {
                                    // Null result
                                    // We may want to display a warning to user (e.g. Toast)

                                    Log.e("[DispoSrchRes] [REST]", "Response error : null body");
                                }
                            }

                            @Override
                            public void onFailure(Call<DispoParkingSearchResult> call, Throwable t) {
                                // Request has failed or is not at expected format
                                // We may want to display a warning to user (e.g. Toast)
                                Log.e("[DispoSrchRes] [REST]", "Response error : " + t.getMessage());
                            }
                        });
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                }
            }
        }, REFRESH_DELAY, TimeUnit.MILLISECONDS);
    }

    /**
     * Permet de faire une recherche en base pour les Disponibilites en fonction d'une liste d'ids et de publier le resultat sur le bus.
     * @param idsObj liste des id pour rechercher les Disponibilites
     */
    public void searchDispoParkingFromDB(ArrayList<Integer> idsObj) {
        String queryWhere = generateQueryWhere(idsObj);
        List<Disponibilite> matchingDispoParkingFromDB = new Select()
                .from(Disponibilite.class)
                .where(queryWhere)
                .execute();

        // Post result as an event
        EventBusManager.BUS.post(new SearchDispoParkingResultEvent(matchingDispoParkingFromDB));
    }

    /**
     * Permet de recuperer une disponibilité en base en fonction d'un id.
     * @param idObj l'id du parking dont on veut recuperer la disponibilite
     */
    public void searchDispoParkingFromDB(Integer idObj) {
        List<Disponibilite> matchingDispoParkingFromDB = new Select()
                .from(Disponibilite.class)
                .where("IdObj = " + idObj)
                .execute();

        // si la dispo n'est pas reliée à son parking, on le set
        for(Disponibilite dispo : matchingDispoParkingFromDB) {
            if(dispo.getParking() == null) {
                Parking matchingParkingFromBdd = new Select()
                        .from(Parking.class)
                        .where("IdObj = " + idObj)
                        .executeSingle();

                // si on a bien un parking qui correspond; on le set a la dispo
                if(matchingParkingFromBdd != null) {
                    dispo.setParking(matchingParkingFromBdd);
                    dispo.save();
                }
            }
        }

        // Post result as an event
        EventBusManager.BUS.post(new SearchDispoParkingResultEvent(matchingDispoParkingFromDB));
    }

    /**
     *  Permet de generer une string pour le where d'une query.
     *  @param idsObj liste des idsObj
     *  @return une chaine de caractere concatenant les ids passes en parametre
     */
    private String generateQueryWhere(ArrayList<Integer>  idsObj) {
        StringBuilder builder = new StringBuilder();
        if(idsObj != null) {
            if(idsObj.size() > 1) {
                builder.append("IdObj in (");
                for (int i = 0; i < idsObj.size() - 1; i++) {
                    builder.append(idsObj.get(i) + ", ");
                }
                builder.append(idsObj.get(idsObj.size() - 1));
                builder.append(")");
            } else if(idsObj.size() == 1){
                builder.append("IdObj = " + idsObj.get(0));
            }
        }

        return builder.toString();
    }

    /**
     * Permet de faire une recherche en base pour les Disponibilites en fonction d'une liste d'ids.
     * @param idsObj liste des id pour rechercher les Disponibilites
     * @return la liste des Disponibilités en fonction des ids passés en parametre
     */
    public List<Disponibilite> getDispoParkingFromDB(ArrayList<Integer> idsObj) {
        String queryWhere = generateQueryWhere(idsObj);
        List<Disponibilite> matchingDispoParkingFromDB = new Select()
                .from(Disponibilite.class)
                .where(queryWhere)
                .execute();
        return matchingDispoParkingFromDB;
    }

    // Service describing the REST APIs
    public interface DispoParkingSearchRESTService {
        @GET("/api/getDisponibiliteParkingsPublics/1.0/DZO7IIKZCTNAZF6/?output=json")
        Call<DispoParkingSearchResult> searchForDispoParking();
    }
}

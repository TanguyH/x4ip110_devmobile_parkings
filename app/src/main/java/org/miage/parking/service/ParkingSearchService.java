package org.miage.parking.service;

import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.miage.parking.event.EventBusManager;
import org.miage.parking.event.SearchParkingResultEvent;
import org.miage.parking.model.dao.Disponibilite;
import org.miage.parking.model.dao.MoyenDePaiement;
import org.miage.parking.model.dao.Parking;
import org.miage.parking.model.ParkingSearchResult;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Classe pour les appels REST pour les Parkings
 */
public class ParkingSearchService {
    // delai pour le scheduler pour les appels a l'API
    private static final long REFRESH_DELAY = 650;
    // delai pour le scheduler pour les appels a la base
    private static final long REFRESH_DELAY_DB = 650;
    public static final ParkingSearchService INSTANCE = new ParkingSearchService();
    private final ParkingSearchRESTService ParkingSearchRESTService;
    private ScheduledExecutorService mScheduler = Executors.newScheduledThreadPool(1);
    private ScheduledFuture mLastScheduleTask;
    // clause join entre parking et moyen de paiement
    private String joinParkingMoyenPaiement = "Parking.IdObj = MoyenDePaiement.IdParking";
    // clause join entre parking et disponibilite
    private String joinParkingDisponibilite = "Parking.IdObj = Disponibilite.IdObj";
    // pour specifier l'order by des resultats des requetes en base
    private String geoName = "GeoName";

    // Constructor
    private ParkingSearchService() {
        // Create GSON Converter that will be used to convert from JSON to Java
        Gson gsonConverter = new GsonBuilder()
                .setLenient()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .excludeFieldsWithoutExposeAnnotation().create();

        // Create Retrofit client
        Retrofit retrofit = new Retrofit.Builder()
                // Using OkHttp as HTTP Client
                .client(new OkHttpClient())
                // Having the following as server URL
                .baseUrl("https://data.nantes.fr")
                // Using GSON to convert from Json to Java
                .addConverterFactory(GsonConverterFactory.create(gsonConverter))
                .build();

        // Use retrofit to generate our REST service code
        ParkingSearchRESTService = retrofit.create(ParkingSearchRESTService.class);
    }

    /**
     * Permet de faire un appel REST pour recuperer les Parkings.
     * @param isModeAvionOn boolean indiquant si l'utilisateur est en mode Avion (HL)
     */
    public void searchParkingFromAddress(final boolean isModeAvionOn) {
        // Cancel last scheduled network call (if any)
        if (mLastScheduleTask != null && !mLastScheduleTask.isDone()) {
            mLastScheduleTask.cancel(true);
        }

        // Schedule a network call in REFRESH_DELAY ms
        mLastScheduleTask = mScheduler.schedule(new Runnable() {
            public void run() {
                // on regarde si on est en mode avion (HL), si c'est le cas pas besoin de faire l'appels
                if (!isModeAvionOn) {
                    // Call to the REST service
                    try {
                        ParkingSearchRESTService.searchForParking("json").enqueue(new Callback<ParkingSearchResult>() {
                            @Override
                            public void onResponse(Call<ParkingSearchResult> call, Response<ParkingSearchResult> response) {
                                // Post an event so that listening activities can update their UI
                                if (response.body() != null && response.body().data != null) {
                                    // Save all results in Database
                                    ActiveAndroid.beginTransaction();
                                    for (Parking parking : response.body().data) {
                                        // on regarde si on a la dispo correspondante en base
                                        Disponibilite matchingParkingDispoFromDB = new Select()
                                                .from(Disponibilite.class)
                                                .where("IdObj = " + parking.getIdObj())
                                                .executeSingle();
                                        // si c'est le cas, on l'ajoute au parking
                                        if(matchingParkingDispoFromDB != null) {
                                            parking.setDisponibilite(matchingParkingDispoFromDB);
                                            parking.getDisponibilite().setParking(parking);
                                            parking.getDisponibilite().save();
                                        }

                                        parking.getGeo().save();
                                        parking.setGeoName(parking.getGeo().getName());
                                        parking.setLatitude(parking.getCoordonnees().get(0));
                                        parking.setLongitude(parking.getCoordonnees().get(1));
                                        parking.setIsParkingRelais(false);

                                        MoyenDePaiement matchingMoyenDePaiementFromDB = new Select()
                                                .from(MoyenDePaiement.class)
                                                .where("IdParking = " + parking.getIdObj())
                                                .executeSingle();
                                        // si c'est le cas, on l'ajoute au parking
                                        if(matchingMoyenDePaiementFromDB != null) {
                                            parking.setMoyenDePaiement(matchingMoyenDePaiementFromDB);
                                            parking.getMoyenDePaiement().setAllAttributs(parking.getMoyenPaiement());
                                            parking.getMoyenDePaiement().save();
                                        } else {
                                            parking.setMoyenDePaiement(new MoyenDePaiement());
                                            parking.getMoyenDePaiement().setAllAttributs(parking.getMoyenPaiement());
                                            parking.getMoyenDePaiement().setIdParking(parking.getIdObj());
                                            parking.getMoyenDePaiement().save();
                                        }

                                        parking.save();
                                    }
                                    ActiveAndroid.setTransactionSuccessful();
                                    ActiveAndroid.endTransaction();
                               } else {
                                    // Null result
                                    // We may want to display a warning to user (e.g. Toast)

                                    Log.e("[ParkingSrchRes] [REST]", "Response error : null body");
                                }
                            }

                            @Override
                            public void onFailure(Call<ParkingSearchResult> call, Throwable t) {
                                // Request has failed or is not at expected format
                                // We may want to display a warning to user (e.g. Toast)
                                Log.e("[ParkingSrchRes] [REST]", "Response error : " + t.getMessage());
                            }
                        });
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                }
            }
        }, REFRESH_DELAY, TimeUnit.MILLISECONDS);
    }

    /**
     * Permet de lancer la recherche des Disponibilites pour les Parkings passes en parametre.
     * @param listParking la liste des Parkings dont les Disponibilites seront recuperees
     * @param isModeAvionOn boolean indiquant si l'utilisateur est en mode Avion (HL)
     */
    private void recupererInfosDisponibilite(List<Parking> listParking, boolean isModeAvionOn) {
        // si on a trouve des parkings
        if(!listParking.isEmpty()) {
            ArrayList<Integer> idsParking = new ArrayList<>();

            //on recupere l'id des parkings que l'on a recupere
            for(Parking p : listParking) {
                idsParking.add(p.getIdObj());
            }

            // on lance la recherche pour recuperer la dispo des parkings recuperes
            if(!idsParking.isEmpty()){
                // si on est en mode avion, le lien n'est pas a jour entre parking et dispo
                if(isModeAvionOn) {
                    // dans ce cas la, on recupere les dispos en base et on met a jour le lien entre le parking et sa dispo
                    List<Disponibilite> dispos = DispoParkingSearchService.INSTANCE.getDispoParkingFromDB(idsParking);
                    for (Parking p : listParking) {
                        for (Disponibilite d : dispos) {
                            if (d.getIdObj() == p.getIdObj()) {
                                p.setDisponibilite(d);
                                p.save();
                                break;
                            }
                        }
                    }
                }

                if(idsParking.size() > 1){
                    DispoParkingSearchService.INSTANCE.searchDispoParkingFromAddress(idsParking, isModeAvionOn);
                } else {
                    DispoParkingSearchService.INSTANCE.searchDispoParkingFromAddress(idsParking.get(0), isModeAvionOn);
                }
            }
        }
    }

    /**
     * Permet de faire une recherche en base pour les Parkings en fonction d'une chaine de caractere.
     * @param nom le nom du parking pour la recherche
     * @param adresse l'adresse du parking pour la recherche
     * @param isCB un boolean indiquant si on souhaite rechercher les parkings autorisants les CB
     * @param isCheque un boolean indiquant si on souhaite rechercher les parkings autorisants les Cheques
     * @param isEspeces un boolean indiquant si on souhaite rechercher les parkings autorisants le paiement par especes
     * @param isTotalGR un boolean indiquant si on souhaite rechercher les parkings avec le moyen de paiement TotalGR
     * @param nbPlaceDispo le seuil de place dispo des parkings que l'on souhaite rechercher
     * @param isModeAvionOn boolean indiquant si l'utilisateur est en mode Avion (HL)
     * @param nomEtAdresse boolean indiquant si l'on veut que le nom ET l'adresse correspondent dans la recherche. Si non renseigné ou null, on fait un OR.
     * @param etMoyens boolean indiquant si l'on veut que la recherche correspond à TOUS les moyens de paiement selectionnés. Si non renseigné, on fait un AND aussi.
     * @param isParkingRelais boolean indiquant si l'on veut que la recherche ajoute les parking relais.
     * @param majDesDispos boolean indiquant si l'on veut faire un appel pour mettre a jour les donnees des disponibilites.
     */
    private void searchParkingFromDB(String nom, String adresse, Boolean isCB, Boolean isCheque, Boolean isEspeces, Boolean isTotalGR, Boolean isFavoris, Integer nbPlaceDispo, boolean isModeAvionOn, final Boolean nomEtAdresse, final Boolean etMoyens, final Boolean isParkingRelais, final Boolean majDesDispos) {
        String queryWhereParking = queryBuilderParking(nom, adresse, isCB, isCheque, isEspeces, isTotalGR, nomEtAdresse, etMoyens);
        String queryWhereDispo = queryBuilderDisponibilite(nbPlaceDispo, isFavoris);
        String queryWhereParkingRelais = queryBuilderParking(isParkingRelais);

        List<Parking> matchingParkingFromDB;

        // si on a des filtres sur les parkings
        if(!queryWhereParking.isEmpty()) {
            // on ajoute le filtre sur le nombre de place
            if(!queryWhereDispo.isEmpty()) {
                if(!queryWhereParkingRelais.isEmpty()){
                // cas 1.1 : filtres sur les dispos et les parkings (sans les parkings relais)
                //on recupere en base les parkings corresponds aux filtres
                matchingParkingFromDB = new Select()
                        .from(Parking.class)
                        .innerJoin(Disponibilite.class)
                        .on(joinParkingDisponibilite)
                        .innerJoin(MoyenDePaiement.class)
                        .on(joinParkingMoyenPaiement)
                        .where("(" + queryWhereParking + ") AND" + queryWhereDispo + " AND " + queryWhereParkingRelais)
                        .orderBy(geoName)
                        .execute();
                } else {
                    // cas 1.2 : filtres sur les dispos et les parkings (avec les parkings relais)
                    //on recupere en base les parkings corresponds aux filtres
                    matchingParkingFromDB = new Select()
                            .from(Parking.class)
                            .innerJoin(Disponibilite.class)
                            .on(joinParkingDisponibilite)
                            .innerJoin(MoyenDePaiement.class)
                            .on(joinParkingMoyenPaiement)
                            .where("("+ queryWhereParking + ") AND" + queryWhereDispo)
                            .orderBy(geoName)
                            .execute();
                }
            } else {
                if(!queryWhereParkingRelais.isEmpty()) {
                    // cas 2.1 : pas de filtre sur les dispos
                    // on recupere en base les parkings (sans les parkings relais) corresponds aux filtres
                    matchingParkingFromDB = new Select()
                            .from(Parking.class)
                            .innerJoin(MoyenDePaiement.class)
                            .on(joinParkingMoyenPaiement)
                            .where("(" + queryWhereParking+ ") AND " + queryWhereParkingRelais)
                            .orderBy(geoName)
                            .execute();
                } else {
                    // cas 2.2 : pas de filtre sur les dispos
                    // on recupere en base les parkings(avec les parkings relais) corresponds aux filtres
                    matchingParkingFromDB = new Select()
                            .from(Parking.class)
                            .innerJoin(MoyenDePaiement.class)
                            .on(joinParkingMoyenPaiement)
                            .where(queryWhereParking)
                            .orderBy(geoName)
                            .execute();
                }
            }
        } else {
            //sinon, on regarde les filtres sur les dispos
            if(!queryWhereDispo.isEmpty()) {
                if(!queryWhereParkingRelais.isEmpty()){
                    // cas 3.1 : pas de filtres sur les parkings (sans les parkings relais) + filtres sur les dispos
                    matchingParkingFromDB = new Select()
                            .from(Parking.class)
                            .innerJoin(Disponibilite.class)
                            .on(joinParkingDisponibilite)
                            .innerJoin(MoyenDePaiement.class)
                            .on(joinParkingMoyenPaiement)
                            .where("(" + queryWhereDispo + ") AND " + queryWhereParkingRelais)
                            .orderBy(geoName)
                            .execute();
                }else{
                    // cas 3.2 : pas de filtres sur les parkings (avec les parkings relais) + filtres sur les dispos
                    matchingParkingFromDB = new Select()
                            .from(Parking.class)
                            .innerJoin(Disponibilite.class)
                            .on(joinParkingDisponibilite)
                            .innerJoin(MoyenDePaiement.class)
                            .on(joinParkingMoyenPaiement)
                            .where(queryWhereDispo)
                            .orderBy(geoName)
                            .execute();

                }
            } else {
                if(!queryWhereParkingRelais.isEmpty()){
                    // cas 4 : pas de filtres : on recupere tout (sans les parkings relais)
                    matchingParkingFromDB = new Select()
                            .from(Parking.class)
                            .where(queryWhereParkingRelais)
                            .orderBy(geoName)
                            .execute();

                }else {
                    // cas 4 : pas de filtres : on recupere tout (avec les parkings relais)
                    matchingParkingFromDB = new Select()
                            .from(Parking.class)
                            .orderBy(geoName)
                            .execute();
                }
            }
        }

        // on lance la recuperation des infos disponibilite (mise à jour des données) pour la liste des parkings retournes
        if(majDesDispos == null || majDesDispos) {
            recupererInfosDisponibilite(matchingParkingFromDB, isModeAvionOn);
        }

        // Post result as an event
        EventBusManager.BUS.post(new SearchParkingResultEvent(matchingParkingFromDB));
    }

    /**
     * Permet de faire une recherche en base pour les Parkings en fonction des differents parametres et avec un scheduler pour ne pas trop solliciter la base
     * @param nom le nom du parking pour la recherche
     * @param adresse l'adresse du parking pour la recherche
     * @param isCB un boolean indiquant si on souhaite rechercher les parkings autorisants les CB
     * @param isCheque un boolean indiquant si on souhaite rechercher les parkings autorisants les Cheques
     * @param isEspeces un boolean indiquant si on souhaite rechercher les parkings autorisants le paiement par especes
     * @param isTotalGR un boolean indiquant si on souhaite rechercher les parkings avec le moyen de paiement TotalGR
     * @param nbPlaceDispo le seuil de place dispo des parkings que l'on souhaite rechercher
     * @param isModeAvionOn boolean indiquant si l'utilisateur est en mode Avion (HL)
     * @param nomEtAdresse boolean indiquant si l'on veut que le nom ET l'adresse correspondent dans la recherche. Si non renseigné ou null, on fait un OR.
     * @param etMoyens boolean indiquant si l'on veut que la recherche correspond à TOUS les moyens de paiement selectionnés. Si non renseigné, on fait un AND aussi.
     * @param isParkingRelais boolean indiquant si l'on veut que la recherche ajoute les parking relais.
     * @param majDesDispos boolean indiquant si l'on veut faire un appel pour mettre a jour les donnees des disponibilites.
     */
    public void searchParkingFromDBWithScheduler(final String nom, final String adresse, final Boolean isCB, final Boolean isCheque, final Boolean isEspeces, final Boolean isTotalGR, final Boolean isFavoris, final Integer nbPlaceDispo, final boolean isModeAvionOn, final Boolean nomEtAdresse, final Boolean etMoyens, final Boolean isParkingRelais, final Boolean majDesDispos) {
        // Cancel last scheduled network call (if any)
        if (mLastScheduleTask != null && !mLastScheduleTask.isDone()) {
            mLastScheduleTask.cancel(true);
        }

        // Schedule a network call in REFRESH_DELAY ms
        mLastScheduleTask = mScheduler.schedule(new Runnable() {
            public void run() {
                // Step 1 : first run a local search from DB and post result
                searchParkingFromDB(nom, adresse, isCB, isCheque, isEspeces, isTotalGR, isFavoris, nbPlaceDispo, isModeAvionOn, nomEtAdresse, etMoyens, isParkingRelais, majDesDispos);
            }
        }, REFRESH_DELAY_DB, TimeUnit.MILLISECONDS);
    }

    /**
     * Permet de construire la clause WHERE avec differents filtres sur les Parkings (nom, adresse, moyens de paiement)
     * @param nom le nom du parking pour la recherche
     * @param adresse l'adresse du parking pour la recherche
     * @param isCB un boolean indiquant si on souhaite rechercher les parkings autorisants les CB
     * @param isCheque un boolean indiquant si on souhaite rechercher les parkings autorisants les Cheques
     * @param isEspeces un boolean indiquant si on souhaite rechercher les parkings autorisants le paiement par especes
     * @param isTotalGR un boolean indiquant si on souhaite rechercher les parkings avec le moyen de paiement TotalGR
     * @param nomEtAdresse boolean indiquant si l'on veut que le nom ET l'adresse correspondent dans la recherche. Si non renseigné ou null, on fait un OR.
     * @param etMoyens boolean indiquant si l'on veut que la recherche correspond à TOUS les moyens de paiement selectionnés. Si non renseigné, on fait un AND aussi
     * @return la clause WHERE avec les différents filtres renseignés
     */
    private String queryBuilderParking(String nom, String adresse, Boolean isCB, Boolean isCheque, Boolean isEspeces, Boolean isTotalGR, Boolean nomEtAdresse, Boolean etMoyens) {
        StringBuilder builder = new StringBuilder();

        // ajout du filtre sur le nom, si celui-ci est renseigne
        if(!nom.isEmpty()) {
            builder.append(" GeoName LIKE '%"+nom+"%' ");
        }
        // ajout du filtre sur l'adresse, si celle-ci est renseigne
        if(!adresse.isEmpty()) {
            if(builder.length() > 0) {
                if(nomEtAdresse != null && nomEtAdresse) {
                    builder.append("AND");
                }else {
                    builder.append("OR");
                }
            }
            builder.append(" Adresse LIKE '%"+adresse+"%' ");
        }

        // ajout du filtre sur les moyens de paiement, si ceux-ci sont renseignes
        builder = queryBuilderAddMoyensDePaiement(builder, isCB, isCheque, isEspeces, isTotalGR, etMoyens);

        return builder.toString();
    }
    /**
     * Permet de construire la clause WHERE avec le parkingRelai
     * @param isParkingRelais un boolean indiquant si on souhaite rechercher les parkings relais
     * @return la clause WHERE avec les différents filtres renseignés
     */
    private String queryBuilderParking(Boolean isParkingRelais) {
        StringBuilder builder = new StringBuilder();

        if(isParkingRelais != null) {
            if (!isParkingRelais) {
                builder.append(" IsParkingRelais = 0");
            }
        }
        return builder.toString();
    }

    /**
     * Permet de construire la clause WHERE pour le filtre sur les moyens de paiement
     * @param builder la clause WHERE a completer
     * @param isCB un boolean indiquant si on souhaite rechercher les parkings autorisants les CB
     * @param isCheque un boolean indiquant si on souhaite rechercher les parkings autorisants les Cheques
     * @param isEspeces un boolean indiquant si on souhaite rechercher les parkings autorisants le paiement par especes
     * @param isTotalGR un boolean indiquant si on souhaite rechercher les parkings avec le moyen de paiement TotalGR
     * @param etMoyens boolean indiquant si l'on veut que la recherche correspond à TOUS les moyens de paiement selectionnés. Si non renseigné, on fait un AND aussi.
     * @return la clause WHERE mis a jour avec le filtre sur les moyens de paiement
     */
    public StringBuilder queryBuilderAddMoyensDePaiement(StringBuilder builder, Boolean isCB, Boolean isCheque, Boolean isEspeces, Boolean isTotalGR, Boolean etMoyens) {
        ArrayList<String> tab = new ArrayList<>();

        // on recupere les moyens de paiement selectionnes
        if(isCB != null && isCB) {
            tab.add("IsCB");
        }
        if(isCheque != null && isCheque) {
            tab.add("IsCheque");
        }
        if(isEspeces != null && isEspeces) {
            tab.add("IsEspeces");
        }
        if(isTotalGR != null && isTotalGR) {
            tab.add("IsTotalGR");
        }

        // s'il y en a, on construit la chaine de caractere qui correspond
        if(!tab.isEmpty()){
            if(builder.length() > 0) {
                builder.append("AND (");
            } else {
                builder.append(" (");
            }
            for(int i = 0; i < tab.size() - 1; i++){
                builder.append(tab.get(i) + " = 1");
                if(etMoyens == null || etMoyens) {
                    builder.append(" AND ");
                } else {
                    builder.append(" OR ");
                }
            }
            builder.append(tab.get(tab.size()-1) + " = 1");
            builder.append(") ");
        }
        return builder;
    }

    /**
     * Permet de construire la clause WHERE pour le filtre sur les propriétés des Disponibilités (le nombre de place disponible, favoris)
     * @param nbPlaceDispo le seuil de place dispo des parkings que l'on souhaite rechercher
     * @param isFavoris un boolean indiquant si on souhaite rechercher les parkings qui ont ete mis en favoris
     * @return la clause WHERE mis a jour avec le filtre sur le nombre de place disponible
     */
    public String queryBuilderDisponibilite(Integer nbPlaceDispo, Boolean isFavoris) {
        StringBuilder builder = new StringBuilder();

        if(nbPlaceDispo != null) {
            builder.append(" GrpDisponible >= " + nbPlaceDispo);
        }

        if(isFavoris != null) {
            if(builder.length() > 0) {
                builder.append(" AND");
            }
            if (isFavoris) {
                builder.append(" Favoris = 1");
            } else {
                builder.append(" Favoris = 0");
            }
        }

        return builder.toString();
    }

    /**
     * Permet de rechercher un parking à partir d'un id
     * @param idObj l'id du parking que l'on souhaite rechercher
     * @return le parking correspondant à l'id passé en paramètre
     */
    public Parking getParkingFromDB(Integer idObj) {
        Parking matchingParkingFromDB = new Select()
                .from(Parking.class)
                .where("IdObj = " + idObj)
                .executeSingle();
        return matchingParkingFromDB;
    }

    /**
     * Permet de recuperer tous les parkings
     * @return la liste de tous les parkings
     */
    public void getAllParkingsFromDB() {
        List<Parking> matchingParkingsFromDB = new Select()
                .from(Parking.class)
                .execute();

        // On publie le resultat sur le bus
        EventBusManager.BUS.post(new SearchParkingResultEvent(matchingParkingsFromDB));
    }

    // Service describing the REST APIs
    public interface ParkingSearchRESTService {
        @GET("/api/publication/24440040400129_NM_NM_00044/LISTE_SERVICES_PKGS_PUB_NM_STBL/content/")
        Call<ParkingSearchResult> searchForParking(@Query("format") String format);
    }
}

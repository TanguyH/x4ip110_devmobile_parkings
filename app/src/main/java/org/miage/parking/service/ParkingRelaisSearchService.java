package org.miage.parking.service;

import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.miage.parking.event.EventBusManager;
import org.miage.parking.event.SearchParkingResultEvent;
import org.miage.parking.model.ParkingSearchResult;
import org.miage.parking.model.dao.Disponibilite;
import org.miage.parking.model.dao.MoyenDePaiement;
import org.miage.parking.model.dao.Parking;
import java.lang.reflect.Modifier;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Classe pour les appels REST pour les Parkings
 */
public class ParkingRelaisSearchService {
    private static final long REFRESH_DELAY = 650;
    public static final ParkingRelaisSearchService INSTANCE = new ParkingRelaisSearchService();
    private final ParkingRelaisSearchRESTService ParkingRelaisSearchRESTService;
    private ScheduledExecutorService mScheduler = Executors.newScheduledThreadPool(1);
    private ScheduledFuture mLastScheduleTask;

    // Constructor
    private ParkingRelaisSearchService() {
        // Create GSON Converter that will be used to convert from JSON to Java
        Gson gsonConverter = new GsonBuilder()
                .setLenient()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .excludeFieldsWithoutExposeAnnotation().create();

        // Create Retrofit client
        Retrofit retrofit = new Retrofit.Builder()
                // Using OkHttp as HTTP Client
                .client(new OkHttpClient())
                // Having the following as server URL
                .baseUrl("https://data.nantes.fr")
                // Using GSON to convert from Json to Java
                .addConverterFactory(GsonConverterFactory.create(gsonConverter))
                .build();

        // Use retrofit to generate our REST service code
        ParkingRelaisSearchRESTService = retrofit.create(ParkingRelaisSearchRESTService.class);
    }

    /**
     * Permet de faire un appel REST pour recuperer les Parkings Relais.
     * @param isModeAvionOn boolean indiquant si l'utilisateur est en mode Avion (HL)
     */
    public void searchParkingFromAddress(final boolean isModeAvionOn) {
        // Cancel last scheduled network call (if any)
        if (mLastScheduleTask != null && !mLastScheduleTask.isDone()) {
            mLastScheduleTask.cancel(true);
        }

        // Schedule a network call in REFRESH_DELAY ms
        mLastScheduleTask = mScheduler.schedule(new Runnable() {
            public void run() {
                // on regarde si on est en mode avion (HL), si c'est le cas pas besoin de faire l'appels
                if (!isModeAvionOn) {
                    // Call to the REST service
                    try {
                        ParkingRelaisSearchRESTService.searchForParking("json").enqueue(new Callback<ParkingSearchResult>() {
                            @Override
                            public void onResponse(Call<ParkingSearchResult> call, Response<ParkingSearchResult> response) {
                                // Post an event so that listening activities can update their UI
                                if (response.body() != null && response.body().data != null) {
                                    // Save all results in Database
                                    ActiveAndroid.beginTransaction();
                                    int compteurDispoFictive = -100;

                                    for (Parking parking : response.body().data) {
                                        // on rajoute une disponibilite fictive
                                        Disponibilite dispo = new Disponibilite();
                                        dispo.setGrpIdentifiant(compteurDispoFictive);
                                        dispo.setIdObj(parking.getIdObj());
                                        dispo.setIsFavoris(false);
                                        dispo.setGrpHorodatage(new Date().toString());
                                        dispo.setGrpNom(parking.getGeo().getName());
                                        dispo.save();
                                        parking.setDisponibilite(dispo);

                                        // pour ne pas risquer d'écraser des disponibilites reelles
                                        // on cree un compteur commencant a -100 pour setter l'identifiant des dispos des parcs relais
                                        compteurDispoFictive -= 1;

                                        parking.getGeo().save();
                                        parking.setGeoName(parking.getGeo().getName());
                                        parking.setLatitude(parking.getCoordonnees().get(0));
                                        parking.setLongitude(parking.getCoordonnees().get(1));
                                        parking.setIsParkingRelais(true);

                                        MoyenDePaiement matchingMoyenDePaiementFromDB = new Select()
                                                .from(MoyenDePaiement.class)
                                                .where("IdParking = " + parking.getIdObj())
                                                .executeSingle();
                                        // si c'est le cas, on l'ajoute au parking
                                        if (matchingMoyenDePaiementFromDB != null) {
                                            parking.setMoyenDePaiement(matchingMoyenDePaiementFromDB);
                                            parking.getMoyenDePaiement().setAllAttributs(parking.getMoyenPaiement());
                                            parking.getMoyenDePaiement().save();
                                        } else {
                                            parking.setMoyenDePaiement(new MoyenDePaiement());
                                            parking.getMoyenDePaiement().setAllAttributs(parking.getMoyenPaiement());
                                            parking.getMoyenDePaiement().setIdParking(parking.getIdObj());
                                            parking.getMoyenDePaiement().save();
                                        }

                                        parking.save();
                                    }
                                    ActiveAndroid.setTransactionSuccessful();
                                    ActiveAndroid.endTransaction();
                                } else {
                                    // Null result
                                    // We may want to display a warning to user (e.g. Toast)

                                    Log.e("[PkgRelSrchRes] [REST]", "Response error : null body");
                                }
                            }

                            @Override
                            public void onFailure(Call<ParkingSearchResult> call, Throwable t) {
                                // Request has failed or is not at expected format
                                // We may want to display a warning to user (e.g. Toast)
                                Log.e("[PkgRelSrchRes] [REST]", "Response error : " + t.getMessage());
                            }
                        });
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                }
            }
        }, REFRESH_DELAY, TimeUnit.MILLISECONDS);
    }

    /**
     * Permet de faire une recherche en base pour recuperer tous les parkings relais.
     */
    public void searchParkingRelaisFromDB() {
        // on cherche en base les equipements se trouvant dans la zone définie
        List<Parking> matchingParkingFromDB = new Select()
                .from(Parking.class)
                .where("IsParkingRelais = 1")
                .execute();

        // Post result as an event
        EventBusManager.BUS.post(new SearchParkingResultEvent(matchingParkingFromDB));
    }

    // Service describing the REST APIs
    public interface ParkingRelaisSearchRESTService {
        @GET("/api/publication/24440040400129_NM_NM_00039/LOC_PARCS_RELAIS_NM_STBL/content/")
        Call<ParkingSearchResult> searchForParking(@Query("format") String format);
    }
}
